﻿namespace HealthcareSystem.View
{
    partial class HrSystemView
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (this.components != null))
            {
                this.components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.usernameTB = new System.Windows.Forms.TextBox();
            this.passwordTB = new System.Windows.Forms.TextBox();
            this.usernameLbl = new System.Windows.Forms.Label();
            this.passwordLbl = new System.Windows.Forms.Label();
            this.loginBtn = new System.Windows.Forms.Button();
            this.registerBtn = new System.Windows.Forms.Button();
            this.phoneTB = new System.Windows.Forms.TextBox();
            this.fnameTB = new System.Windows.Forms.TextBox();
            this.lnameTB = new System.Windows.Forms.TextBox();
            this.fnameLbl = new System.Windows.Forms.Label();
            this.lnameLbl = new System.Windows.Forms.Label();
            this.birthdateLbl = new System.Windows.Forms.Label();
            this.phoneLbl = new System.Windows.Forms.Label();
            this.nurseRB = new System.Windows.Forms.RadioButton();
            this.adminRB = new System.Windows.Forms.RadioButton();
            this.birthDateDTP = new System.Windows.Forms.DateTimePicker();
            this.regUsernameLbl = new System.Windows.Forms.Label();
            this.regPasswordLbl = new System.Windows.Forms.Label();
            this.regPasswordTB = new System.Windows.Forms.TextBox();
            this.regUsernameTB = new System.Windows.Forms.TextBox();
            this.cancelBtn = new System.Windows.Forms.Button();
            this.regRegisterBtn = new System.Windows.Forms.Button();
            this.regErrorLbl = new System.Windows.Forms.Label();
            this.regPanel = new System.Windows.Forms.Panel();
            this.registerStateComboBox = new System.Windows.Forms.ComboBox();
            this.registerZipCodeLabel = new System.Windows.Forms.Label();
            this.registerStateLabel = new System.Windows.Forms.Label();
            this.registerCityLabel = new System.Windows.Forms.Label();
            this.registerLine2Label = new System.Windows.Forms.Label();
            this.registerZipCodeTextBox = new System.Windows.Forms.TextBox();
            this.registerCityTextBox = new System.Windows.Forms.TextBox();
            this.registerLine2TextBox = new System.Windows.Forms.TextBox();
            this.registerLine1Label = new System.Windows.Forms.Label();
            this.registerLine1TextBox = new System.Windows.Forms.TextBox();
            this.registerAddressLabel = new System.Windows.Forms.Label();
            this.registerGenderLbl = new System.Windows.Forms.Label();
            this.registerRoleLbl = new System.Windows.Forms.Label();
            this.registerGenderPanel = new System.Windows.Forms.Panel();
            this.registerMaleRB = new System.Windows.Forms.RadioButton();
            this.registerFemaleRB = new System.Windows.Forms.RadioButton();
            this.rolePanel = new System.Windows.Forms.Panel();
            this.loginPanel = new System.Windows.Forms.Panel();
            this.newPatientPanel = new System.Windows.Forms.Panel();
            this.patientStateComboBox = new System.Windows.Forms.ComboBox();
            this.patientZipCodeLabel = new System.Windows.Forms.Label();
            this.patientStateLabel = new System.Windows.Forms.Label();
            this.patientCityLabel = new System.Windows.Forms.Label();
            this.patientAddressLine2Label = new System.Windows.Forms.Label();
            this.patientAddress2TextBoc = new System.Windows.Forms.Label();
            this.patientZipCodeTextBox = new System.Windows.Forms.TextBox();
            this.patientCityTextBox = new System.Windows.Forms.TextBox();
            this.patientAddress2TextBox = new System.Windows.Forms.TextBox();
            this.streetLabel = new System.Windows.Forms.Label();
            this.patientFemaleRB = new System.Windows.Forms.RadioButton();
            this.patientMaleRB = new System.Windows.Forms.RadioButton();
            this.patientGenderLbl = new System.Windows.Forms.Label();
            this.patientErrorLabel = new System.Windows.Forms.Label();
            this.newPatientButton = new System.Windows.Forms.Button();
            this.patientPhoneLabel = new System.Windows.Forms.Label();
            this.patientPhoneTextBox = new System.Windows.Forms.TextBox();
            this.patientStreetTextbox = new System.Windows.Forms.TextBox();
            this.patientAddressLabel = new System.Windows.Forms.Label();
            this.newPatientDateTimePicker = new System.Windows.Forms.DateTimePicker();
            this.patientBirthdateLabel = new System.Windows.Forms.Label();
            this.patientLNameTextBox = new System.Windows.Forms.TextBox();
            this.patientLNameLabel = new System.Windows.Forms.Label();
            this.patientFNameLabel = new System.Windows.Forms.Label();
            this.patientFNameTextBox = new System.Windows.Forms.TextBox();
            this.newPatientLabel = new System.Windows.Forms.Label();
            this.nurseAppTabControl = new System.Windows.Forms.TabControl();
            this.patientTab = new System.Windows.Forms.TabPage();
            this.panel1 = new System.Windows.Forms.Panel();
            this.searchPatientDGV = new System.Windows.Forms.DataGridView();
            this.searchPatientBtn = new System.Windows.Forms.Button();
            this.label5 = new System.Windows.Forms.Label();
            this.label8 = new System.Windows.Forms.Label();
            this.searchPatientDOBDTP = new System.Windows.Forms.DateTimePicker();
            this.searchPatientDOBLbl = new System.Windows.Forms.Label();
            this.searchPatientLnameTB = new System.Windows.Forms.TextBox();
            this.searchPatientLnameLbl = new System.Windows.Forms.Label();
            this.searchPatientFnameLbl = new System.Windows.Forms.Label();
            this.searchPatientFnameTB = new System.Windows.Forms.TextBox();
            this.searchPatientLbl = new System.Windows.Forms.Label();
            this.appointmentTab = new System.Windows.Forms.TabPage();
            this.panel2 = new System.Windows.Forms.Panel();
            this.searchVisitInfoErrLbL = new System.Windows.Forms.Label();
            this.searchVisitInfoBtn = new System.Windows.Forms.Button();
            this.searchVisitInfoDGV = new System.Windows.Forms.DataGridView();
            this.searchVisitInfoTB = new System.Windows.Forms.TextBox();
            this.searchVisitInfoLbl = new System.Windows.Forms.Label();
            this.searchVisitLbl = new System.Windows.Forms.Label();
            this.appointmentPanel = new System.Windows.Forms.Panel();
            this.apptReasonRTB = new System.Windows.Forms.RichTextBox();
            this.apptCancelBtn = new System.Windows.Forms.Button();
            this.apptReasonLbl = new System.Windows.Forms.Label();
            this.apptAddBtn = new System.Windows.Forms.Button();
            this.patientListBox = new System.Windows.Forms.ListBox();
            this.patientListLbl = new System.Windows.Forms.Label();
            this.apptDTP = new System.Windows.Forms.DateTimePicker();
            this.apptDateLbl = new System.Windows.Forms.Label();
            this.apptDoctorLbl = new System.Windows.Forms.Label();
            this.doctorListBox = new System.Windows.Forms.ListBox();
            this.addAppointmentLbl = new System.Windows.Forms.Label();
            this.checksTab = new System.Windows.Forms.TabPage();
            this.performChecksPanel = new System.Windows.Forms.Panel();
            this.checksValidationLabel = new System.Windows.Forms.Label();
            this.checkCancelBtn = new System.Windows.Forms.Button();
            this.checkSaveBtn = new System.Windows.Forms.Button();
            this.diagnosisTB = new System.Windows.Forms.TextBox();
            this.pulseTB = new System.Windows.Forms.TextBox();
            this.diastolicTB = new System.Windows.Forms.TextBox();
            this.systolicTB = new System.Windows.Forms.TextBox();
            this.tempTB = new System.Windows.Forms.TextBox();
            this.diastolicLbl = new System.Windows.Forms.Label();
            this.systolicLbl = new System.Windows.Forms.Label();
            this.tempLbl = new System.Windows.Forms.Label();
            this.pulseLbl = new System.Windows.Forms.Label();
            this.diagnosisLbl = new System.Windows.Forms.Label();
            this.testLbl = new System.Windows.Forms.Label();
            this.hepBCB = new System.Windows.Forms.CheckBox();
            this.hepACB = new System.Windows.Forms.CheckBox();
            this.ldlCB = new System.Windows.Forms.CheckBox();
            this.whitebloodcellCB = new System.Windows.Forms.CheckBox();
            this.patientChecksDGV = new System.Windows.Forms.DataGridView();
            this.updatesTab = new System.Windows.Forms.TabPage();
            this.updateTestAndDiagnosisPanel = new System.Windows.Forms.Panel();
            this.performedOnLbl = new System.Windows.Forms.Label();
            this.performedOnDTP = new System.Windows.Forms.DateTimePicker();
            this.testTypeListBox = new System.Windows.Forms.ListBox();
            this.testTypeLbl = new System.Windows.Forms.Label();
            this.updateErrLbl = new System.Windows.Forms.Label();
            this.cancelUpdateBtn = new System.Windows.Forms.Button();
            this.updateTestBtn = new System.Windows.Forms.Button();
            this.updatedDiagnosisLbl = new System.Windows.Forms.Label();
            this.updatedDiagnosisTB = new System.Windows.Forms.TextBox();
            this.testResultLbl = new System.Windows.Forms.Label();
            this.diagnosisIdLbl = new System.Windows.Forms.Label();
            this.diagnosisIdTB = new System.Windows.Forms.TextBox();
            this.testResultTB = new System.Windows.Forms.TextBox();
            this.updateVisitInfo = new System.Windows.Forms.Label();
            this.currentUserLbl = new System.Windows.Forms.Label();
            this.logoutBtn = new System.Windows.Forms.Button();
            this.adminAppPanel = new System.Windows.Forms.Panel();
            this.enterSQLBtn = new System.Windows.Forms.Button();
            this.adminSQLDGV = new System.Windows.Forms.DataGridView();
            this.adminSQLRTB = new System.Windows.Forms.RichTextBox();
            this.adminAppLbl = new System.Windows.Forms.Label();
            this.adminTabControl = new System.Windows.Forms.TabControl();
            this.queryTabPage = new System.Windows.Forms.TabPage();
            this.datePickerTabPage = new System.Windows.Forms.TabPage();
            this.startDateTimePicker = new System.Windows.Forms.DateTimePicker();
            this.endDateTimePicker = new System.Windows.Forms.DateTimePicker();
            this.startDateLbl = new System.Windows.Forms.Label();
            this.endDateLbl = new System.Windows.Forms.Label();
            this.betweenDatesBtn = new System.Windows.Forms.Button();
            this.dateVisitDGV = new System.Windows.Forms.DataGridView();
            this.regPanel.SuspendLayout();
            this.registerGenderPanel.SuspendLayout();
            this.rolePanel.SuspendLayout();
            this.loginPanel.SuspendLayout();
            this.newPatientPanel.SuspendLayout();
            this.nurseAppTabControl.SuspendLayout();
            this.patientTab.SuspendLayout();
            this.panel1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.searchPatientDGV)).BeginInit();
            this.appointmentTab.SuspendLayout();
            this.panel2.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.searchVisitInfoDGV)).BeginInit();
            this.appointmentPanel.SuspendLayout();
            this.checksTab.SuspendLayout();
            this.performChecksPanel.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.patientChecksDGV)).BeginInit();
            this.updatesTab.SuspendLayout();
            this.updateTestAndDiagnosisPanel.SuspendLayout();
            this.adminAppPanel.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.adminSQLDGV)).BeginInit();
            this.adminTabControl.SuspendLayout();
            this.queryTabPage.SuspendLayout();
            this.datePickerTabPage.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dateVisitDGV)).BeginInit();
            this.SuspendLayout();
            // 
            // usernameTB
            // 
            this.usernameTB.Location = new System.Drawing.Point(100, 18);
            this.usernameTB.Name = "usernameTB";
            this.usernameTB.Size = new System.Drawing.Size(167, 20);
            this.usernameTB.TabIndex = 1;
            // 
            // passwordTB
            // 
            this.passwordTB.Location = new System.Drawing.Point(100, 44);
            this.passwordTB.Name = "passwordTB";
            this.passwordTB.PasswordChar = '*';
            this.passwordTB.Size = new System.Drawing.Size(167, 20);
            this.passwordTB.TabIndex = 2;
            // 
            // usernameLbl
            // 
            this.usernameLbl.AutoSize = true;
            this.usernameLbl.Location = new System.Drawing.Point(36, 21);
            this.usernameLbl.Name = "usernameLbl";
            this.usernameLbl.Size = new System.Drawing.Size(58, 13);
            this.usernameLbl.TabIndex = 3;
            this.usernameLbl.Text = "Username:";
            // 
            // passwordLbl
            // 
            this.passwordLbl.AutoSize = true;
            this.passwordLbl.Location = new System.Drawing.Point(37, 47);
            this.passwordLbl.Name = "passwordLbl";
            this.passwordLbl.Size = new System.Drawing.Size(56, 13);
            this.passwordLbl.TabIndex = 4;
            this.passwordLbl.Text = "Password:";
            // 
            // loginBtn
            // 
            this.loginBtn.Location = new System.Drawing.Point(99, 70);
            this.loginBtn.Name = "loginBtn";
            this.loginBtn.Size = new System.Drawing.Size(75, 23);
            this.loginBtn.TabIndex = 5;
            this.loginBtn.Text = "Login";
            this.loginBtn.UseVisualStyleBackColor = true;
            this.loginBtn.Click += new System.EventHandler(this.loginBtn_Click);
            // 
            // registerBtn
            // 
            this.registerBtn.Location = new System.Drawing.Point(191, 70);
            this.registerBtn.Name = "registerBtn";
            this.registerBtn.Size = new System.Drawing.Size(75, 23);
            this.registerBtn.TabIndex = 6;
            this.registerBtn.Text = "Register";
            this.registerBtn.UseVisualStyleBackColor = true;
            this.registerBtn.Click += new System.EventHandler(this.registerBtn_Click);
            // 
            // phoneTB
            // 
            this.phoneTB.Location = new System.Drawing.Point(84, 241);
            this.phoneTB.Margin = new System.Windows.Forms.Padding(2, 2, 2, 2);
            this.phoneTB.Name = "phoneTB";
            this.phoneTB.Size = new System.Drawing.Size(189, 20);
            this.phoneTB.TabIndex = 13;
            // 
            // fnameTB
            // 
            this.fnameTB.Location = new System.Drawing.Point(84, 2);
            this.fnameTB.Margin = new System.Windows.Forms.Padding(2, 2, 2, 2);
            this.fnameTB.Name = "fnameTB";
            this.fnameTB.Size = new System.Drawing.Size(189, 20);
            this.fnameTB.TabIndex = 9;
            // 
            // lnameTB
            // 
            this.lnameTB.Location = new System.Drawing.Point(84, 28);
            this.lnameTB.Margin = new System.Windows.Forms.Padding(2, 2, 2, 2);
            this.lnameTB.Name = "lnameTB";
            this.lnameTB.Size = new System.Drawing.Size(189, 20);
            this.lnameTB.TabIndex = 10;
            // 
            // fnameLbl
            // 
            this.fnameLbl.AutoSize = true;
            this.fnameLbl.Location = new System.Drawing.Point(20, 6);
            this.fnameLbl.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.fnameLbl.Name = "fnameLbl";
            this.fnameLbl.Size = new System.Drawing.Size(60, 13);
            this.fnameLbl.TabIndex = 12;
            this.fnameLbl.Text = "First Name:";
            // 
            // lnameLbl
            // 
            this.lnameLbl.AutoSize = true;
            this.lnameLbl.Location = new System.Drawing.Point(20, 32);
            this.lnameLbl.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.lnameLbl.Name = "lnameLbl";
            this.lnameLbl.Size = new System.Drawing.Size(61, 13);
            this.lnameLbl.TabIndex = 13;
            this.lnameLbl.Text = "Last Name:";
            // 
            // birthdateLbl
            // 
            this.birthdateLbl.AutoSize = true;
            this.birthdateLbl.Location = new System.Drawing.Point(23, 58);
            this.birthdateLbl.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.birthdateLbl.Name = "birthdateLbl";
            this.birthdateLbl.Size = new System.Drawing.Size(57, 13);
            this.birthdateLbl.TabIndex = 14;
            this.birthdateLbl.Text = "Birth Date:";
            // 
            // phoneLbl
            // 
            this.phoneLbl.AutoSize = true;
            this.phoneLbl.Location = new System.Drawing.Point(1, 245);
            this.phoneLbl.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.phoneLbl.Name = "phoneLbl";
            this.phoneLbl.Size = new System.Drawing.Size(81, 13);
            this.phoneLbl.TabIndex = 16;
            this.phoneLbl.Text = "Phone Number:";
            // 
            // nurseRB
            // 
            this.nurseRB.AutoSize = true;
            this.nurseRB.Location = new System.Drawing.Point(10, 3);
            this.nurseRB.Margin = new System.Windows.Forms.Padding(2, 2, 2, 2);
            this.nurseRB.Name = "nurseRB";
            this.nurseRB.Size = new System.Drawing.Size(53, 17);
            this.nurseRB.TabIndex = 14;
            this.nurseRB.TabStop = true;
            this.nurseRB.Text = "Nurse";
            this.nurseRB.UseVisualStyleBackColor = true;
            // 
            // adminRB
            // 
            this.adminRB.AutoSize = true;
            this.adminRB.Location = new System.Drawing.Point(96, 3);
            this.adminRB.Margin = new System.Windows.Forms.Padding(2, 2, 2, 2);
            this.adminRB.Name = "adminRB";
            this.adminRB.Size = new System.Drawing.Size(54, 17);
            this.adminRB.TabIndex = 15;
            this.adminRB.TabStop = true;
            this.adminRB.Text = "Admin";
            this.adminRB.UseVisualStyleBackColor = true;
            // 
            // birthDateDTP
            // 
            this.birthDateDTP.Location = new System.Drawing.Point(84, 54);
            this.birthDateDTP.Margin = new System.Windows.Forms.Padding(2, 2, 2, 2);
            this.birthDateDTP.Name = "birthDateDTP";
            this.birthDateDTP.Size = new System.Drawing.Size(189, 20);
            this.birthDateDTP.TabIndex = 11;
            // 
            // regUsernameLbl
            // 
            this.regUsernameLbl.AutoSize = true;
            this.regUsernameLbl.Location = new System.Drawing.Point(21, 345);
            this.regUsernameLbl.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.regUsernameLbl.Name = "regUsernameLbl";
            this.regUsernameLbl.Size = new System.Drawing.Size(58, 13);
            this.regUsernameLbl.TabIndex = 20;
            this.regUsernameLbl.Text = "Username:";
            // 
            // regPasswordLbl
            // 
            this.regPasswordLbl.AutoSize = true;
            this.regPasswordLbl.Location = new System.Drawing.Point(25, 371);
            this.regPasswordLbl.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.regPasswordLbl.Name = "regPasswordLbl";
            this.regPasswordLbl.Size = new System.Drawing.Size(56, 13);
            this.regPasswordLbl.TabIndex = 21;
            this.regPasswordLbl.Text = "Password:";
            // 
            // regPasswordTB
            // 
            this.regPasswordTB.Location = new System.Drawing.Point(84, 367);
            this.regPasswordTB.Name = "regPasswordTB";
            this.regPasswordTB.PasswordChar = '*';
            this.regPasswordTB.Size = new System.Drawing.Size(189, 20);
            this.regPasswordTB.TabIndex = 17;
            // 
            // regUsernameTB
            // 
            this.regUsernameTB.Location = new System.Drawing.Point(84, 341);
            this.regUsernameTB.Name = "regUsernameTB";
            this.regUsernameTB.Size = new System.Drawing.Size(189, 20);
            this.regUsernameTB.TabIndex = 16;
            // 
            // cancelBtn
            // 
            this.cancelBtn.Location = new System.Drawing.Point(197, 397);
            this.cancelBtn.Name = "cancelBtn";
            this.cancelBtn.Size = new System.Drawing.Size(75, 23);
            this.cancelBtn.TabIndex = 19;
            this.cancelBtn.Text = "Cancel";
            this.cancelBtn.UseVisualStyleBackColor = true;
            this.cancelBtn.Click += new System.EventHandler(this.cancelBtn_Click);
            // 
            // regRegisterBtn
            // 
            this.regRegisterBtn.Location = new System.Drawing.Point(84, 397);
            this.regRegisterBtn.Name = "regRegisterBtn";
            this.regRegisterBtn.Size = new System.Drawing.Size(75, 23);
            this.regRegisterBtn.TabIndex = 18;
            this.regRegisterBtn.Text = "Register";
            this.regRegisterBtn.UseVisualStyleBackColor = true;
            this.regRegisterBtn.Click += new System.EventHandler(this.regRegisterBtn_Click);
            // 
            // regErrorLbl
            // 
            this.regErrorLbl.AutoSize = true;
            this.regErrorLbl.Location = new System.Drawing.Point(81, 432);
            this.regErrorLbl.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.regErrorLbl.Name = "regErrorLbl";
            this.regErrorLbl.Size = new System.Drawing.Size(0, 13);
            this.regErrorLbl.TabIndex = 26;
            // 
            // regPanel
            // 
            this.regPanel.Controls.Add(this.registerStateComboBox);
            this.regPanel.Controls.Add(this.registerZipCodeLabel);
            this.regPanel.Controls.Add(this.registerStateLabel);
            this.regPanel.Controls.Add(this.registerCityLabel);
            this.regPanel.Controls.Add(this.registerLine2Label);
            this.regPanel.Controls.Add(this.registerZipCodeTextBox);
            this.regPanel.Controls.Add(this.registerCityTextBox);
            this.regPanel.Controls.Add(this.registerLine2TextBox);
            this.regPanel.Controls.Add(this.registerLine1Label);
            this.regPanel.Controls.Add(this.registerLine1TextBox);
            this.regPanel.Controls.Add(this.registerAddressLabel);
            this.regPanel.Controls.Add(this.registerGenderLbl);
            this.regPanel.Controls.Add(this.registerRoleLbl);
            this.regPanel.Controls.Add(this.registerGenderPanel);
            this.regPanel.Controls.Add(this.rolePanel);
            this.regPanel.Controls.Add(this.regErrorLbl);
            this.regPanel.Controls.Add(this.fnameTB);
            this.regPanel.Controls.Add(this.cancelBtn);
            this.regPanel.Controls.Add(this.regRegisterBtn);
            this.regPanel.Controls.Add(this.phoneTB);
            this.regPanel.Controls.Add(this.regPasswordTB);
            this.regPanel.Controls.Add(this.lnameTB);
            this.regPanel.Controls.Add(this.regUsernameTB);
            this.regPanel.Controls.Add(this.fnameLbl);
            this.regPanel.Controls.Add(this.regPasswordLbl);
            this.regPanel.Controls.Add(this.lnameLbl);
            this.regPanel.Controls.Add(this.regUsernameLbl);
            this.regPanel.Controls.Add(this.birthdateLbl);
            this.regPanel.Controls.Add(this.birthDateDTP);
            this.regPanel.Controls.Add(this.phoneLbl);
            this.regPanel.Location = new System.Drawing.Point(227, 11);
            this.regPanel.Margin = new System.Windows.Forms.Padding(2, 2, 2, 2);
            this.regPanel.Name = "regPanel";
            this.regPanel.Size = new System.Drawing.Size(285, 469);
            this.regPanel.TabIndex = 27;
            // 
            // registerStateComboBox
            // 
            this.registerStateComboBox.FormattingEnabled = true;
            this.registerStateComboBox.Location = new System.Drawing.Point(86, 191);
            this.registerStateComboBox.Name = "registerStateComboBox";
            this.registerStateComboBox.Size = new System.Drawing.Size(47, 21);
            this.registerStateComboBox.TabIndex = 42;
            // 
            // registerZipCodeLabel
            // 
            this.registerZipCodeLabel.AutoSize = true;
            this.registerZipCodeLabel.Location = new System.Drawing.Point(138, 200);
            this.registerZipCodeLabel.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.registerZipCodeLabel.Name = "registerZipCodeLabel";
            this.registerZipCodeLabel.Size = new System.Drawing.Size(53, 13);
            this.registerZipCodeLabel.TabIndex = 41;
            this.registerZipCodeLabel.Text = "Zip Code:";
            // 
            // registerStateLabel
            // 
            this.registerStateLabel.AutoSize = true;
            this.registerStateLabel.Location = new System.Drawing.Point(40, 200);
            this.registerStateLabel.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.registerStateLabel.Name = "registerStateLabel";
            this.registerStateLabel.Size = new System.Drawing.Size(35, 13);
            this.registerStateLabel.TabIndex = 40;
            this.registerStateLabel.Text = "State:";
            // 
            // registerCityLabel
            // 
            this.registerCityLabel.AutoSize = true;
            this.registerCityLabel.Location = new System.Drawing.Point(48, 170);
            this.registerCityLabel.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.registerCityLabel.Name = "registerCityLabel";
            this.registerCityLabel.Size = new System.Drawing.Size(27, 13);
            this.registerCityLabel.TabIndex = 39;
            this.registerCityLabel.Text = "City:";
            // 
            // registerLine2Label
            // 
            this.registerLine2Label.AutoSize = true;
            this.registerLine2Label.Location = new System.Drawing.Point(36, 140);
            this.registerLine2Label.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.registerLine2Label.Name = "registerLine2Label";
            this.registerLine2Label.Size = new System.Drawing.Size(39, 13);
            this.registerLine2Label.TabIndex = 38;
            this.registerLine2Label.Text = "Line 2:";
            // 
            // registerZipCodeTextBox
            // 
            this.registerZipCodeTextBox.Location = new System.Drawing.Point(195, 197);
            this.registerZipCodeTextBox.Margin = new System.Windows.Forms.Padding(2, 2, 2, 2);
            this.registerZipCodeTextBox.Name = "registerZipCodeTextBox";
            this.registerZipCodeTextBox.Size = new System.Drawing.Size(77, 20);
            this.registerZipCodeTextBox.TabIndex = 37;
            // 
            // registerCityTextBox
            // 
            this.registerCityTextBox.Location = new System.Drawing.Point(86, 167);
            this.registerCityTextBox.Margin = new System.Windows.Forms.Padding(2, 2, 2, 2);
            this.registerCityTextBox.Name = "registerCityTextBox";
            this.registerCityTextBox.Size = new System.Drawing.Size(186, 20);
            this.registerCityTextBox.TabIndex = 35;
            // 
            // registerLine2TextBox
            // 
            this.registerLine2TextBox.Location = new System.Drawing.Point(86, 137);
            this.registerLine2TextBox.Margin = new System.Windows.Forms.Padding(2, 2, 2, 2);
            this.registerLine2TextBox.Name = "registerLine2TextBox";
            this.registerLine2TextBox.Size = new System.Drawing.Size(186, 20);
            this.registerLine2TextBox.TabIndex = 34;
            // 
            // registerLine1Label
            // 
            this.registerLine1Label.AutoSize = true;
            this.registerLine1Label.Location = new System.Drawing.Point(37, 112);
            this.registerLine1Label.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.registerLine1Label.Name = "registerLine1Label";
            this.registerLine1Label.Size = new System.Drawing.Size(39, 13);
            this.registerLine1Label.TabIndex = 33;
            this.registerLine1Label.Text = "Line 1:";
            // 
            // registerLine1TextBox
            // 
            this.registerLine1TextBox.Location = new System.Drawing.Point(86, 109);
            this.registerLine1TextBox.Margin = new System.Windows.Forms.Padding(2, 2, 2, 2);
            this.registerLine1TextBox.Name = "registerLine1TextBox";
            this.registerLine1TextBox.Size = new System.Drawing.Size(186, 20);
            this.registerLine1TextBox.TabIndex = 32;
            // 
            // registerAddressLabel
            // 
            this.registerAddressLabel.AutoSize = true;
            this.registerAddressLabel.Location = new System.Drawing.Point(27, 87);
            this.registerAddressLabel.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.registerAddressLabel.Name = "registerAddressLabel";
            this.registerAddressLabel.Size = new System.Drawing.Size(48, 13);
            this.registerAddressLabel.TabIndex = 31;
            this.registerAddressLabel.Text = "Address:";
            // 
            // registerGenderLbl
            // 
            this.registerGenderLbl.AutoSize = true;
            this.registerGenderLbl.Location = new System.Drawing.Point(34, 304);
            this.registerGenderLbl.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.registerGenderLbl.Name = "registerGenderLbl";
            this.registerGenderLbl.Size = new System.Drawing.Size(45, 13);
            this.registerGenderLbl.TabIndex = 30;
            this.registerGenderLbl.Text = "Gender:";
            // 
            // registerRoleLbl
            // 
            this.registerRoleLbl.AutoSize = true;
            this.registerRoleLbl.Location = new System.Drawing.Point(47, 278);
            this.registerRoleLbl.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.registerRoleLbl.Name = "registerRoleLbl";
            this.registerRoleLbl.Size = new System.Drawing.Size(32, 13);
            this.registerRoleLbl.TabIndex = 29;
            this.registerRoleLbl.Text = "Role:";
            // 
            // registerGenderPanel
            // 
            this.registerGenderPanel.Controls.Add(this.registerMaleRB);
            this.registerGenderPanel.Controls.Add(this.registerFemaleRB);
            this.registerGenderPanel.Location = new System.Drawing.Point(84, 297);
            this.registerGenderPanel.Name = "registerGenderPanel";
            this.registerGenderPanel.Size = new System.Drawing.Size(189, 23);
            this.registerGenderPanel.TabIndex = 28;
            // 
            // registerMaleRB
            // 
            this.registerMaleRB.AutoSize = true;
            this.registerMaleRB.Location = new System.Drawing.Point(10, 3);
            this.registerMaleRB.Margin = new System.Windows.Forms.Padding(2, 2, 2, 2);
            this.registerMaleRB.Name = "registerMaleRB";
            this.registerMaleRB.Size = new System.Drawing.Size(48, 17);
            this.registerMaleRB.TabIndex = 14;
            this.registerMaleRB.TabStop = true;
            this.registerMaleRB.Text = "Male";
            this.registerMaleRB.UseVisualStyleBackColor = true;
            // 
            // registerFemaleRB
            // 
            this.registerFemaleRB.AutoSize = true;
            this.registerFemaleRB.Location = new System.Drawing.Point(96, 3);
            this.registerFemaleRB.Margin = new System.Windows.Forms.Padding(2, 2, 2, 2);
            this.registerFemaleRB.Name = "registerFemaleRB";
            this.registerFemaleRB.Size = new System.Drawing.Size(59, 17);
            this.registerFemaleRB.TabIndex = 15;
            this.registerFemaleRB.TabStop = true;
            this.registerFemaleRB.Text = "Female";
            this.registerFemaleRB.UseVisualStyleBackColor = true;
            // 
            // rolePanel
            // 
            this.rolePanel.Controls.Add(this.nurseRB);
            this.rolePanel.Controls.Add(this.adminRB);
            this.rolePanel.Location = new System.Drawing.Point(84, 268);
            this.rolePanel.Name = "rolePanel";
            this.rolePanel.Size = new System.Drawing.Size(189, 23);
            this.rolePanel.TabIndex = 27;
            // 
            // loginPanel
            // 
            this.loginPanel.Controls.Add(this.usernameTB);
            this.loginPanel.Controls.Add(this.registerBtn);
            this.loginPanel.Controls.Add(this.passwordTB);
            this.loginPanel.Controls.Add(this.loginBtn);
            this.loginPanel.Controls.Add(this.usernameLbl);
            this.loginPanel.Controls.Add(this.passwordLbl);
            this.loginPanel.Location = new System.Drawing.Point(216, 158);
            this.loginPanel.Margin = new System.Windows.Forms.Padding(2, 2, 2, 2);
            this.loginPanel.Name = "loginPanel";
            this.loginPanel.Size = new System.Drawing.Size(310, 114);
            this.loginPanel.TabIndex = 28;
            // 
            // newPatientPanel
            // 
            this.newPatientPanel.Controls.Add(this.patientStateComboBox);
            this.newPatientPanel.Controls.Add(this.patientZipCodeLabel);
            this.newPatientPanel.Controls.Add(this.patientStateLabel);
            this.newPatientPanel.Controls.Add(this.patientCityLabel);
            this.newPatientPanel.Controls.Add(this.patientAddressLine2Label);
            this.newPatientPanel.Controls.Add(this.patientAddress2TextBoc);
            this.newPatientPanel.Controls.Add(this.patientZipCodeTextBox);
            this.newPatientPanel.Controls.Add(this.patientCityTextBox);
            this.newPatientPanel.Controls.Add(this.patientAddress2TextBox);
            this.newPatientPanel.Controls.Add(this.streetLabel);
            this.newPatientPanel.Controls.Add(this.patientFemaleRB);
            this.newPatientPanel.Controls.Add(this.patientMaleRB);
            this.newPatientPanel.Controls.Add(this.patientGenderLbl);
            this.newPatientPanel.Controls.Add(this.patientErrorLabel);
            this.newPatientPanel.Controls.Add(this.newPatientButton);
            this.newPatientPanel.Controls.Add(this.patientPhoneLabel);
            this.newPatientPanel.Controls.Add(this.patientPhoneTextBox);
            this.newPatientPanel.Controls.Add(this.patientStreetTextbox);
            this.newPatientPanel.Controls.Add(this.patientAddressLabel);
            this.newPatientPanel.Controls.Add(this.newPatientDateTimePicker);
            this.newPatientPanel.Controls.Add(this.patientBirthdateLabel);
            this.newPatientPanel.Controls.Add(this.patientLNameTextBox);
            this.newPatientPanel.Controls.Add(this.patientLNameLabel);
            this.newPatientPanel.Controls.Add(this.patientFNameLabel);
            this.newPatientPanel.Controls.Add(this.patientFNameTextBox);
            this.newPatientPanel.Controls.Add(this.newPatientLabel);
            this.newPatientPanel.Location = new System.Drawing.Point(5, 5);
            this.newPatientPanel.Margin = new System.Windows.Forms.Padding(2, 2, 2, 2);
            this.newPatientPanel.Name = "newPatientPanel";
            this.newPatientPanel.Size = new System.Drawing.Size(305, 422);
            this.newPatientPanel.TabIndex = 29;
            // 
            // patientStateComboBox
            // 
            this.patientStateComboBox.FormattingEnabled = true;
            this.patientStateComboBox.Location = new System.Drawing.Point(97, 237);
            this.patientStateComboBox.Name = "patientStateComboBox";
            this.patientStateComboBox.Size = new System.Drawing.Size(47, 21);
            this.patientStateComboBox.TabIndex = 26;
            // 
            // patientZipCodeLabel
            // 
            this.patientZipCodeLabel.AutoSize = true;
            this.patientZipCodeLabel.Location = new System.Drawing.Point(149, 239);
            this.patientZipCodeLabel.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.patientZipCodeLabel.Name = "patientZipCodeLabel";
            this.patientZipCodeLabel.Size = new System.Drawing.Size(53, 13);
            this.patientZipCodeLabel.TabIndex = 25;
            this.patientZipCodeLabel.Text = "Zip Code:";
            // 
            // patientStateLabel
            // 
            this.patientStateLabel.AutoSize = true;
            this.patientStateLabel.Location = new System.Drawing.Point(51, 239);
            this.patientStateLabel.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.patientStateLabel.Name = "patientStateLabel";
            this.patientStateLabel.Size = new System.Drawing.Size(35, 13);
            this.patientStateLabel.TabIndex = 24;
            this.patientStateLabel.Text = "State:";
            // 
            // patientCityLabel
            // 
            this.patientCityLabel.AutoSize = true;
            this.patientCityLabel.Location = new System.Drawing.Point(59, 209);
            this.patientCityLabel.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.patientCityLabel.Name = "patientCityLabel";
            this.patientCityLabel.Size = new System.Drawing.Size(27, 13);
            this.patientCityLabel.TabIndex = 23;
            this.patientCityLabel.Text = "City:";
            // 
            // patientAddressLine2Label
            // 
            this.patientAddressLine2Label.AutoSize = true;
            this.patientAddressLine2Label.Location = new System.Drawing.Point(47, 179);
            this.patientAddressLine2Label.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.patientAddressLine2Label.Name = "patientAddressLine2Label";
            this.patientAddressLine2Label.Size = new System.Drawing.Size(39, 13);
            this.patientAddressLine2Label.TabIndex = 22;
            this.patientAddressLine2Label.Text = "Line 2:";
            // 
            // patientAddress2TextBoc
            // 
            this.patientAddress2TextBoc.AutoSize = true;
            this.patientAddress2TextBoc.Location = new System.Drawing.Point(2, 180);
            this.patientAddress2TextBoc.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.patientAddress2TextBoc.Name = "patientAddress2TextBoc";
            this.patientAddress2TextBoc.Size = new System.Drawing.Size(0, 13);
            this.patientAddress2TextBoc.TabIndex = 21;
            // 
            // patientZipCodeTextBox
            // 
            this.patientZipCodeTextBox.Location = new System.Drawing.Point(206, 236);
            this.patientZipCodeTextBox.Margin = new System.Windows.Forms.Padding(2, 2, 2, 2);
            this.patientZipCodeTextBox.Name = "patientZipCodeTextBox";
            this.patientZipCodeTextBox.Size = new System.Drawing.Size(77, 20);
            this.patientZipCodeTextBox.TabIndex = 20;
            // 
            // patientCityTextBox
            // 
            this.patientCityTextBox.Location = new System.Drawing.Point(97, 206);
            this.patientCityTextBox.Margin = new System.Windows.Forms.Padding(2, 2, 2, 2);
            this.patientCityTextBox.Name = "patientCityTextBox";
            this.patientCityTextBox.Size = new System.Drawing.Size(186, 20);
            this.patientCityTextBox.TabIndex = 18;
            // 
            // patientAddress2TextBox
            // 
            this.patientAddress2TextBox.Location = new System.Drawing.Point(97, 176);
            this.patientAddress2TextBox.Margin = new System.Windows.Forms.Padding(2, 2, 2, 2);
            this.patientAddress2TextBox.Name = "patientAddress2TextBox";
            this.patientAddress2TextBox.Size = new System.Drawing.Size(186, 20);
            this.patientAddress2TextBox.TabIndex = 17;
            // 
            // streetLabel
            // 
            this.streetLabel.AutoSize = true;
            this.streetLabel.Location = new System.Drawing.Point(48, 151);
            this.streetLabel.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.streetLabel.Name = "streetLabel";
            this.streetLabel.Size = new System.Drawing.Size(39, 13);
            this.streetLabel.TabIndex = 16;
            this.streetLabel.Text = "Line 1:";
            // 
            // patientFemaleRB
            // 
            this.patientFemaleRB.AutoSize = true;
            this.patientFemaleRB.Location = new System.Drawing.Point(183, 323);
            this.patientFemaleRB.Name = "patientFemaleRB";
            this.patientFemaleRB.Size = new System.Drawing.Size(59, 17);
            this.patientFemaleRB.TabIndex = 15;
            this.patientFemaleRB.TabStop = true;
            this.patientFemaleRB.Text = "Female";
            this.patientFemaleRB.UseVisualStyleBackColor = true;
            // 
            // patientMaleRB
            // 
            this.patientMaleRB.AutoSize = true;
            this.patientMaleRB.Location = new System.Drawing.Point(102, 324);
            this.patientMaleRB.Name = "patientMaleRB";
            this.patientMaleRB.Size = new System.Drawing.Size(48, 17);
            this.patientMaleRB.TabIndex = 14;
            this.patientMaleRB.TabStop = true;
            this.patientMaleRB.Text = "Male";
            this.patientMaleRB.UseVisualStyleBackColor = true;
            // 
            // patientGenderLbl
            // 
            this.patientGenderLbl.AutoSize = true;
            this.patientGenderLbl.Location = new System.Drawing.Point(42, 322);
            this.patientGenderLbl.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.patientGenderLbl.Name = "patientGenderLbl";
            this.patientGenderLbl.Size = new System.Drawing.Size(45, 13);
            this.patientGenderLbl.TabIndex = 13;
            this.patientGenderLbl.Text = "Gender:";
            // 
            // patientErrorLabel
            // 
            this.patientErrorLabel.AutoSize = true;
            this.patientErrorLabel.Location = new System.Drawing.Point(94, 396);
            this.patientErrorLabel.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.patientErrorLabel.Name = "patientErrorLabel";
            this.patientErrorLabel.Size = new System.Drawing.Size(0, 13);
            this.patientErrorLabel.TabIndex = 12;
            // 
            // newPatientButton
            // 
            this.newPatientButton.Location = new System.Drawing.Point(97, 353);
            this.newPatientButton.Margin = new System.Windows.Forms.Padding(2, 2, 2, 2);
            this.newPatientButton.Name = "newPatientButton";
            this.newPatientButton.Size = new System.Drawing.Size(127, 28);
            this.newPatientButton.TabIndex = 11;
            this.newPatientButton.Text = "Add New Patient";
            this.newPatientButton.UseVisualStyleBackColor = true;
            this.newPatientButton.Click += new System.EventHandler(this.newPatientButton_Click);
            // 
            // patientPhoneLabel
            // 
            this.patientPhoneLabel.AutoSize = true;
            this.patientPhoneLabel.Location = new System.Drawing.Point(6, 292);
            this.patientPhoneLabel.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.patientPhoneLabel.Name = "patientPhoneLabel";
            this.patientPhoneLabel.Size = new System.Drawing.Size(81, 13);
            this.patientPhoneLabel.TabIndex = 10;
            this.patientPhoneLabel.Text = "Phone Number:";
            // 
            // patientPhoneTextBox
            // 
            this.patientPhoneTextBox.Location = new System.Drawing.Point(97, 292);
            this.patientPhoneTextBox.Margin = new System.Windows.Forms.Padding(2, 2, 2, 2);
            this.patientPhoneTextBox.Name = "patientPhoneTextBox";
            this.patientPhoneTextBox.Size = new System.Drawing.Size(189, 20);
            this.patientPhoneTextBox.TabIndex = 9;
            // 
            // patientStreetTextbox
            // 
            this.patientStreetTextbox.Location = new System.Drawing.Point(97, 148);
            this.patientStreetTextbox.Margin = new System.Windows.Forms.Padding(2, 2, 2, 2);
            this.patientStreetTextbox.Name = "patientStreetTextbox";
            this.patientStreetTextbox.Size = new System.Drawing.Size(186, 20);
            this.patientStreetTextbox.TabIndex = 8;
            // 
            // patientAddressLabel
            // 
            this.patientAddressLabel.AutoSize = true;
            this.patientAddressLabel.Location = new System.Drawing.Point(38, 126);
            this.patientAddressLabel.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.patientAddressLabel.Name = "patientAddressLabel";
            this.patientAddressLabel.Size = new System.Drawing.Size(48, 13);
            this.patientAddressLabel.TabIndex = 7;
            this.patientAddressLabel.Text = "Address:";
            // 
            // newPatientDateTimePicker
            // 
            this.newPatientDateTimePicker.Location = new System.Drawing.Point(99, 97);
            this.newPatientDateTimePicker.Margin = new System.Windows.Forms.Padding(2, 2, 2, 2);
            this.newPatientDateTimePicker.Name = "newPatientDateTimePicker";
            this.newPatientDateTimePicker.Size = new System.Drawing.Size(189, 20);
            this.newPatientDateTimePicker.TabIndex = 6;
            // 
            // patientBirthdateLabel
            // 
            this.patientBirthdateLabel.AutoSize = true;
            this.patientBirthdateLabel.Location = new System.Drawing.Point(30, 97);
            this.patientBirthdateLabel.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.patientBirthdateLabel.Name = "patientBirthdateLabel";
            this.patientBirthdateLabel.Size = new System.Drawing.Size(57, 13);
            this.patientBirthdateLabel.TabIndex = 5;
            this.patientBirthdateLabel.Text = "Birth Date:";
            // 
            // patientLNameTextBox
            // 
            this.patientLNameTextBox.Location = new System.Drawing.Point(99, 67);
            this.patientLNameTextBox.Margin = new System.Windows.Forms.Padding(2, 2, 2, 2);
            this.patientLNameTextBox.Name = "patientLNameTextBox";
            this.patientLNameTextBox.Size = new System.Drawing.Size(189, 20);
            this.patientLNameTextBox.TabIndex = 4;
            // 
            // patientLNameLabel
            // 
            this.patientLNameLabel.AutoSize = true;
            this.patientLNameLabel.Location = new System.Drawing.Point(26, 67);
            this.patientLNameLabel.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.patientLNameLabel.Name = "patientLNameLabel";
            this.patientLNameLabel.Size = new System.Drawing.Size(61, 13);
            this.patientLNameLabel.TabIndex = 3;
            this.patientLNameLabel.Text = "Last Name:";
            // 
            // patientFNameLabel
            // 
            this.patientFNameLabel.AutoSize = true;
            this.patientFNameLabel.Location = new System.Drawing.Point(26, 39);
            this.patientFNameLabel.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.patientFNameLabel.Name = "patientFNameLabel";
            this.patientFNameLabel.Size = new System.Drawing.Size(60, 13);
            this.patientFNameLabel.TabIndex = 2;
            this.patientFNameLabel.Text = "First Name:";
            // 
            // patientFNameTextBox
            // 
            this.patientFNameTextBox.Location = new System.Drawing.Point(99, 39);
            this.patientFNameTextBox.Margin = new System.Windows.Forms.Padding(2, 2, 2, 2);
            this.patientFNameTextBox.Name = "patientFNameTextBox";
            this.patientFNameTextBox.Size = new System.Drawing.Size(189, 20);
            this.patientFNameTextBox.TabIndex = 1;
            // 
            // newPatientLabel
            // 
            this.newPatientLabel.AutoSize = true;
            this.newPatientLabel.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.newPatientLabel.Location = new System.Drawing.Point(7, 6);
            this.newPatientLabel.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.newPatientLabel.Name = "newPatientLabel";
            this.newPatientLabel.Size = new System.Drawing.Size(140, 20);
            this.newPatientLabel.TabIndex = 0;
            this.newPatientLabel.Text = "Add a New Patient";
            // 
            // nurseAppTabControl
            // 
            this.nurseAppTabControl.Controls.Add(this.patientTab);
            this.nurseAppTabControl.Controls.Add(this.appointmentTab);
            this.nurseAppTabControl.Controls.Add(this.checksTab);
            this.nurseAppTabControl.Controls.Add(this.updatesTab);
            this.nurseAppTabControl.Location = new System.Drawing.Point(13, 19);
            this.nurseAppTabControl.Name = "nurseAppTabControl";
            this.nurseAppTabControl.SelectedIndex = 0;
            this.nurseAppTabControl.Size = new System.Drawing.Size(715, 441);
            this.nurseAppTabControl.TabIndex = 30;
            this.nurseAppTabControl.Click += new System.EventHandler(this.appointmentTab_Click);
            // 
            // patientTab
            // 
            this.patientTab.Controls.Add(this.panel1);
            this.patientTab.Controls.Add(this.newPatientPanel);
            this.patientTab.Location = new System.Drawing.Point(4, 22);
            this.patientTab.Name = "patientTab";
            this.patientTab.Padding = new System.Windows.Forms.Padding(3, 3, 3, 3);
            this.patientTab.Size = new System.Drawing.Size(707, 415);
            this.patientTab.TabIndex = 0;
            this.patientTab.Text = "Patient";
            this.patientTab.UseVisualStyleBackColor = true;
            // 
            // panel1
            // 
            this.panel1.Controls.Add(this.searchPatientDGV);
            this.panel1.Controls.Add(this.searchPatientBtn);
            this.panel1.Controls.Add(this.label5);
            this.panel1.Controls.Add(this.label8);
            this.panel1.Controls.Add(this.searchPatientDOBDTP);
            this.panel1.Controls.Add(this.searchPatientDOBLbl);
            this.panel1.Controls.Add(this.searchPatientLnameTB);
            this.panel1.Controls.Add(this.searchPatientLnameLbl);
            this.panel1.Controls.Add(this.searchPatientFnameLbl);
            this.panel1.Controls.Add(this.searchPatientFnameTB);
            this.panel1.Controls.Add(this.searchPatientLbl);
            this.panel1.Location = new System.Drawing.Point(342, 10);
            this.panel1.Margin = new System.Windows.Forms.Padding(2, 2, 2, 2);
            this.panel1.Name = "panel1";
            this.panel1.Size = new System.Drawing.Size(305, 400);
            this.panel1.TabIndex = 30;
            // 
            // searchPatientDGV
            // 
            this.searchPatientDGV.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.searchPatientDGV.Location = new System.Drawing.Point(29, 133);
            this.searchPatientDGV.Name = "searchPatientDGV";
            this.searchPatientDGV.RowHeadersVisible = false;
            this.searchPatientDGV.Size = new System.Drawing.Size(259, 202);
            this.searchPatientDGV.TabIndex = 23;
            // 
            // searchPatientBtn
            // 
            this.searchPatientBtn.Location = new System.Drawing.Point(97, 349);
            this.searchPatientBtn.Margin = new System.Windows.Forms.Padding(2, 2, 2, 2);
            this.searchPatientBtn.Name = "searchPatientBtn";
            this.searchPatientBtn.Size = new System.Drawing.Size(127, 28);
            this.searchPatientBtn.TabIndex = 22;
            this.searchPatientBtn.Text = "Search";
            this.searchPatientBtn.UseVisualStyleBackColor = true;
            this.searchPatientBtn.Click += new System.EventHandler(this.searchPatientBtn_Click);
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.Location = new System.Drawing.Point(2, 180);
            this.label5.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(0, 13);
            this.label5.TabIndex = 21;
            // 
            // label8
            // 
            this.label8.AutoSize = true;
            this.label8.Location = new System.Drawing.Point(94, 396);
            this.label8.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.label8.Name = "label8";
            this.label8.Size = new System.Drawing.Size(0, 13);
            this.label8.TabIndex = 12;
            // 
            // searchPatientDOBDTP
            // 
            this.searchPatientDOBDTP.Location = new System.Drawing.Point(99, 97);
            this.searchPatientDOBDTP.Margin = new System.Windows.Forms.Padding(2, 2, 2, 2);
            this.searchPatientDOBDTP.Name = "searchPatientDOBDTP";
            this.searchPatientDOBDTP.Size = new System.Drawing.Size(189, 20);
            this.searchPatientDOBDTP.TabIndex = 6;
            // 
            // searchPatientDOBLbl
            // 
            this.searchPatientDOBLbl.AutoSize = true;
            this.searchPatientDOBLbl.Location = new System.Drawing.Point(30, 97);
            this.searchPatientDOBLbl.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.searchPatientDOBLbl.Name = "searchPatientDOBLbl";
            this.searchPatientDOBLbl.Size = new System.Drawing.Size(57, 13);
            this.searchPatientDOBLbl.TabIndex = 5;
            this.searchPatientDOBLbl.Text = "Birth Date:";
            // 
            // searchPatientLnameTB
            // 
            this.searchPatientLnameTB.Location = new System.Drawing.Point(99, 67);
            this.searchPatientLnameTB.Margin = new System.Windows.Forms.Padding(2, 2, 2, 2);
            this.searchPatientLnameTB.Name = "searchPatientLnameTB";
            this.searchPatientLnameTB.Size = new System.Drawing.Size(189, 20);
            this.searchPatientLnameTB.TabIndex = 4;
            // 
            // searchPatientLnameLbl
            // 
            this.searchPatientLnameLbl.AutoSize = true;
            this.searchPatientLnameLbl.Location = new System.Drawing.Point(26, 67);
            this.searchPatientLnameLbl.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.searchPatientLnameLbl.Name = "searchPatientLnameLbl";
            this.searchPatientLnameLbl.Size = new System.Drawing.Size(61, 13);
            this.searchPatientLnameLbl.TabIndex = 3;
            this.searchPatientLnameLbl.Text = "Last Name:";
            // 
            // searchPatientFnameLbl
            // 
            this.searchPatientFnameLbl.AutoSize = true;
            this.searchPatientFnameLbl.Location = new System.Drawing.Point(26, 39);
            this.searchPatientFnameLbl.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.searchPatientFnameLbl.Name = "searchPatientFnameLbl";
            this.searchPatientFnameLbl.Size = new System.Drawing.Size(60, 13);
            this.searchPatientFnameLbl.TabIndex = 2;
            this.searchPatientFnameLbl.Text = "First Name:";
            // 
            // searchPatientFnameTB
            // 
            this.searchPatientFnameTB.Location = new System.Drawing.Point(99, 39);
            this.searchPatientFnameTB.Margin = new System.Windows.Forms.Padding(2, 2, 2, 2);
            this.searchPatientFnameTB.Name = "searchPatientFnameTB";
            this.searchPatientFnameTB.Size = new System.Drawing.Size(189, 20);
            this.searchPatientFnameTB.TabIndex = 1;
            // 
            // searchPatientLbl
            // 
            this.searchPatientLbl.AutoSize = true;
            this.searchPatientLbl.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.searchPatientLbl.Location = new System.Drawing.Point(7, 6);
            this.searchPatientLbl.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.searchPatientLbl.Name = "searchPatientLbl";
            this.searchPatientLbl.Size = new System.Drawing.Size(122, 20);
            this.searchPatientLbl.TabIndex = 0;
            this.searchPatientLbl.Text = "Search Patients";
            // 
            // appointmentTab
            // 
            this.appointmentTab.Controls.Add(this.panel2);
            this.appointmentTab.Controls.Add(this.appointmentPanel);
            this.appointmentTab.Location = new System.Drawing.Point(4, 22);
            this.appointmentTab.Name = "appointmentTab";
            this.appointmentTab.Padding = new System.Windows.Forms.Padding(3, 3, 3, 3);
            this.appointmentTab.Size = new System.Drawing.Size(707, 415);
            this.appointmentTab.TabIndex = 1;
            this.appointmentTab.Text = "Appointment";
            this.appointmentTab.UseVisualStyleBackColor = true;
            // 
            // panel2
            // 
            this.panel2.Controls.Add(this.searchVisitInfoErrLbL);
            this.panel2.Controls.Add(this.searchVisitInfoBtn);
            this.panel2.Controls.Add(this.searchVisitInfoDGV);
            this.panel2.Controls.Add(this.searchVisitInfoTB);
            this.panel2.Controls.Add(this.searchVisitInfoLbl);
            this.panel2.Controls.Add(this.searchVisitLbl);
            this.panel2.Location = new System.Drawing.Point(262, 7);
            this.panel2.Margin = new System.Windows.Forms.Padding(2, 2, 2, 2);
            this.panel2.Name = "panel2";
            this.panel2.Size = new System.Drawing.Size(443, 407);
            this.panel2.TabIndex = 4;
            // 
            // searchVisitInfoErrLbL
            // 
            this.searchVisitInfoErrLbL.AutoSize = true;
            this.searchVisitInfoErrLbL.Location = new System.Drawing.Point(279, 42);
            this.searchVisitInfoErrLbL.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.searchVisitInfoErrLbL.Name = "searchVisitInfoErrLbL";
            this.searchVisitInfoErrLbL.Size = new System.Drawing.Size(0, 13);
            this.searchVisitInfoErrLbL.TabIndex = 6;
            // 
            // searchVisitInfoBtn
            // 
            this.searchVisitInfoBtn.Location = new System.Drawing.Point(229, 38);
            this.searchVisitInfoBtn.Margin = new System.Windows.Forms.Padding(2, 2, 2, 2);
            this.searchVisitInfoBtn.Name = "searchVisitInfoBtn";
            this.searchVisitInfoBtn.Size = new System.Drawing.Size(50, 21);
            this.searchVisitInfoBtn.TabIndex = 5;
            this.searchVisitInfoBtn.Text = "Search";
            this.searchVisitInfoBtn.UseVisualStyleBackColor = true;
            this.searchVisitInfoBtn.Click += new System.EventHandler(this.searchVisitInfoBtn_Click);
            // 
            // searchVisitInfoDGV
            // 
            this.searchVisitInfoDGV.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.searchVisitInfoDGV.Location = new System.Drawing.Point(11, 70);
            this.searchVisitInfoDGV.Margin = new System.Windows.Forms.Padding(2, 2, 2, 2);
            this.searchVisitInfoDGV.Name = "searchVisitInfoDGV";
            this.searchVisitInfoDGV.RowTemplate.Height = 28;
            this.searchVisitInfoDGV.Size = new System.Drawing.Size(421, 327);
            this.searchVisitInfoDGV.TabIndex = 4;
            this.searchVisitInfoDGV.DataBindingComplete += new System.Windows.Forms.DataGridViewBindingCompleteEventHandler(this.searchVisitInfoDGV_DataBindingComplete);
            // 
            // searchVisitInfoTB
            // 
            this.searchVisitInfoTB.Location = new System.Drawing.Point(106, 40);
            this.searchVisitInfoTB.Margin = new System.Windows.Forms.Padding(2, 2, 2, 2);
            this.searchVisitInfoTB.Name = "searchVisitInfoTB";
            this.searchVisitInfoTB.Size = new System.Drawing.Size(107, 20);
            this.searchVisitInfoTB.TabIndex = 3;
            // 
            // searchVisitInfoLbl
            // 
            this.searchVisitInfoLbl.AutoSize = true;
            this.searchVisitInfoLbl.Location = new System.Drawing.Point(9, 40);
            this.searchVisitInfoLbl.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.searchVisitInfoLbl.Name = "searchVisitInfoLbl";
            this.searchVisitInfoLbl.Size = new System.Drawing.Size(93, 13);
            this.searchVisitInfoLbl.TabIndex = 2;
            this.searchVisitInfoLbl.Text = "Enter a patient ID:";
            // 
            // searchVisitLbl
            // 
            this.searchVisitLbl.AutoSize = true;
            this.searchVisitLbl.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.searchVisitLbl.Location = new System.Drawing.Point(5, 9);
            this.searchVisitLbl.Name = "searchVisitLbl";
            this.searchVisitLbl.Size = new System.Drawing.Size(208, 20);
            this.searchVisitLbl.TabIndex = 1;
            this.searchVisitLbl.Text = "Search Appointment Details";
            // 
            // appointmentPanel
            // 
            this.appointmentPanel.Controls.Add(this.apptReasonRTB);
            this.appointmentPanel.Controls.Add(this.apptCancelBtn);
            this.appointmentPanel.Controls.Add(this.apptReasonLbl);
            this.appointmentPanel.Controls.Add(this.apptAddBtn);
            this.appointmentPanel.Controls.Add(this.patientListBox);
            this.appointmentPanel.Controls.Add(this.patientListLbl);
            this.appointmentPanel.Controls.Add(this.apptDTP);
            this.appointmentPanel.Controls.Add(this.apptDateLbl);
            this.appointmentPanel.Controls.Add(this.apptDoctorLbl);
            this.appointmentPanel.Controls.Add(this.doctorListBox);
            this.appointmentPanel.Controls.Add(this.addAppointmentLbl);
            this.appointmentPanel.Location = new System.Drawing.Point(7, 7);
            this.appointmentPanel.Name = "appointmentPanel";
            this.appointmentPanel.Size = new System.Drawing.Size(250, 296);
            this.appointmentPanel.TabIndex = 3;
            // 
            // apptReasonRTB
            // 
            this.apptReasonRTB.Location = new System.Drawing.Point(60, 201);
            this.apptReasonRTB.Name = "apptReasonRTB";
            this.apptReasonRTB.Size = new System.Drawing.Size(183, 57);
            this.apptReasonRTB.TabIndex = 9;
            this.apptReasonRTB.Text = "";
            // 
            // apptCancelBtn
            // 
            this.apptCancelBtn.Location = new System.Drawing.Point(168, 264);
            this.apptCancelBtn.Name = "apptCancelBtn";
            this.apptCancelBtn.Size = new System.Drawing.Size(75, 23);
            this.apptCancelBtn.TabIndex = 2;
            this.apptCancelBtn.Text = "Cancel";
            this.apptCancelBtn.UseVisualStyleBackColor = true;
            // 
            // apptReasonLbl
            // 
            this.apptReasonLbl.AutoSize = true;
            this.apptReasonLbl.Location = new System.Drawing.Point(7, 201);
            this.apptReasonLbl.Name = "apptReasonLbl";
            this.apptReasonLbl.Size = new System.Drawing.Size(47, 13);
            this.apptReasonLbl.TabIndex = 8;
            this.apptReasonLbl.Text = "Reason:";
            // 
            // apptAddBtn
            // 
            this.apptAddBtn.Location = new System.Drawing.Point(60, 264);
            this.apptAddBtn.Name = "apptAddBtn";
            this.apptAddBtn.Size = new System.Drawing.Size(75, 23);
            this.apptAddBtn.TabIndex = 1;
            this.apptAddBtn.Text = "Add";
            this.apptAddBtn.UseVisualStyleBackColor = true;
            this.apptAddBtn.Click += new System.EventHandler(this.apptAddBtn_Click);
            // 
            // patientListBox
            // 
            this.patientListBox.FormattingEnabled = true;
            this.patientListBox.Location = new System.Drawing.Point(60, 130);
            this.patientListBox.Name = "patientListBox";
            this.patientListBox.Size = new System.Drawing.Size(183, 56);
            this.patientListBox.TabIndex = 6;
            this.patientListBox.Format += new System.Windows.Forms.ListControlConvertEventHandler(this.patientListBox_Format);
            // 
            // patientListLbl
            // 
            this.patientListLbl.AutoSize = true;
            this.patientListLbl.Location = new System.Drawing.Point(9, 130);
            this.patientListLbl.Name = "patientListLbl";
            this.patientListLbl.Size = new System.Drawing.Size(43, 13);
            this.patientListLbl.TabIndex = 5;
            this.patientListLbl.Text = "Patient:";
            // 
            // apptDTP
            // 
            this.apptDTP.Location = new System.Drawing.Point(60, 93);
            this.apptDTP.Name = "apptDTP";
            this.apptDTP.Size = new System.Drawing.Size(183, 20);
            this.apptDTP.TabIndex = 4;
            // 
            // apptDateLbl
            // 
            this.apptDateLbl.AutoSize = true;
            this.apptDateLbl.Location = new System.Drawing.Point(21, 93);
            this.apptDateLbl.Name = "apptDateLbl";
            this.apptDateLbl.Size = new System.Drawing.Size(33, 13);
            this.apptDateLbl.TabIndex = 3;
            this.apptDateLbl.Text = "Date:";
            // 
            // apptDoctorLbl
            // 
            this.apptDoctorLbl.AutoSize = true;
            this.apptDoctorLbl.Location = new System.Drawing.Point(12, 38);
            this.apptDoctorLbl.Name = "apptDoctorLbl";
            this.apptDoctorLbl.Size = new System.Drawing.Size(42, 13);
            this.apptDoctorLbl.TabIndex = 2;
            this.apptDoctorLbl.Text = "Doctor:";
            // 
            // doctorListBox
            // 
            this.doctorListBox.FormattingEnabled = true;
            this.doctorListBox.Location = new System.Drawing.Point(60, 38);
            this.doctorListBox.Name = "doctorListBox";
            this.doctorListBox.Size = new System.Drawing.Size(183, 43);
            this.doctorListBox.TabIndex = 1;
            this.doctorListBox.Format += new System.Windows.Forms.ListControlConvertEventHandler(this.doctorListBox_Format);
            // 
            // addAppointmentLbl
            // 
            this.addAppointmentLbl.AutoSize = true;
            this.addAppointmentLbl.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.addAppointmentLbl.Location = new System.Drawing.Point(12, 9);
            this.addAppointmentLbl.Name = "addAppointmentLbl";
            this.addAppointmentLbl.Size = new System.Drawing.Size(133, 20);
            this.addAppointmentLbl.TabIndex = 0;
            this.addAppointmentLbl.Text = "Add Appointment";
            // 
            // checksTab
            // 
            this.checksTab.Controls.Add(this.performChecksPanel);
            this.checksTab.Controls.Add(this.patientChecksDGV);
            this.checksTab.Location = new System.Drawing.Point(4, 22);
            this.checksTab.Margin = new System.Windows.Forms.Padding(2, 2, 2, 2);
            this.checksTab.Name = "checksTab";
            this.checksTab.Padding = new System.Windows.Forms.Padding(2, 2, 2, 2);
            this.checksTab.Size = new System.Drawing.Size(707, 415);
            this.checksTab.TabIndex = 2;
            this.checksTab.Text = "Checks";
            this.checksTab.UseVisualStyleBackColor = true;
            this.checksTab.Layout += new System.Windows.Forms.LayoutEventHandler(this.checksTab_Layout);
            // 
            // performChecksPanel
            // 
            this.performChecksPanel.Controls.Add(this.checksValidationLabel);
            this.performChecksPanel.Controls.Add(this.checkCancelBtn);
            this.performChecksPanel.Controls.Add(this.checkSaveBtn);
            this.performChecksPanel.Controls.Add(this.diagnosisTB);
            this.performChecksPanel.Controls.Add(this.pulseTB);
            this.performChecksPanel.Controls.Add(this.diastolicTB);
            this.performChecksPanel.Controls.Add(this.systolicTB);
            this.performChecksPanel.Controls.Add(this.tempTB);
            this.performChecksPanel.Controls.Add(this.diastolicLbl);
            this.performChecksPanel.Controls.Add(this.systolicLbl);
            this.performChecksPanel.Controls.Add(this.tempLbl);
            this.performChecksPanel.Controls.Add(this.pulseLbl);
            this.performChecksPanel.Controls.Add(this.diagnosisLbl);
            this.performChecksPanel.Controls.Add(this.testLbl);
            this.performChecksPanel.Controls.Add(this.hepBCB);
            this.performChecksPanel.Controls.Add(this.hepACB);
            this.performChecksPanel.Controls.Add(this.ldlCB);
            this.performChecksPanel.Controls.Add(this.whitebloodcellCB);
            this.performChecksPanel.Location = new System.Drawing.Point(14, 13);
            this.performChecksPanel.Margin = new System.Windows.Forms.Padding(2, 2, 2, 2);
            this.performChecksPanel.Name = "performChecksPanel";
            this.performChecksPanel.Size = new System.Drawing.Size(377, 346);
            this.performChecksPanel.TabIndex = 3;
            // 
            // checksValidationLabel
            // 
            this.checksValidationLabel.AutoSize = true;
            this.checksValidationLabel.Location = new System.Drawing.Point(186, 317);
            this.checksValidationLabel.Name = "checksValidationLabel";
            this.checksValidationLabel.Size = new System.Drawing.Size(0, 13);
            this.checksValidationLabel.TabIndex = 17;
            this.checksValidationLabel.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // checkCancelBtn
            // 
            this.checkCancelBtn.Location = new System.Drawing.Point(225, 281);
            this.checkCancelBtn.Margin = new System.Windows.Forms.Padding(2, 2, 2, 2);
            this.checkCancelBtn.Name = "checkCancelBtn";
            this.checkCancelBtn.Size = new System.Drawing.Size(50, 23);
            this.checkCancelBtn.TabIndex = 16;
            this.checkCancelBtn.Text = "Cancel";
            this.checkCancelBtn.UseVisualStyleBackColor = true;
            this.checkCancelBtn.Click += new System.EventHandler(this.checkCancelBtn_Click);
            // 
            // checkSaveBtn
            // 
            this.checkSaveBtn.Location = new System.Drawing.Point(99, 281);
            this.checkSaveBtn.Margin = new System.Windows.Forms.Padding(2, 2, 2, 2);
            this.checkSaveBtn.Name = "checkSaveBtn";
            this.checkSaveBtn.Size = new System.Drawing.Size(50, 23);
            this.checkSaveBtn.TabIndex = 15;
            this.checkSaveBtn.Text = "Save";
            this.checkSaveBtn.UseVisualStyleBackColor = true;
            this.checkSaveBtn.Click += new System.EventHandler(this.checkSaveBtn_Click);
            // 
            // diagnosisTB
            // 
            this.diagnosisTB.Location = new System.Drawing.Point(136, 149);
            this.diagnosisTB.Margin = new System.Windows.Forms.Padding(2, 2, 2, 2);
            this.diagnosisTB.Name = "diagnosisTB";
            this.diagnosisTB.Size = new System.Drawing.Size(139, 20);
            this.diagnosisTB.TabIndex = 14;
            // 
            // pulseTB
            // 
            this.pulseTB.Location = new System.Drawing.Point(136, 123);
            this.pulseTB.Margin = new System.Windows.Forms.Padding(2, 2, 2, 2);
            this.pulseTB.Name = "pulseTB";
            this.pulseTB.Size = new System.Drawing.Size(139, 20);
            this.pulseTB.TabIndex = 13;
            // 
            // diastolicTB
            // 
            this.diastolicTB.Location = new System.Drawing.Point(136, 44);
            this.diastolicTB.Margin = new System.Windows.Forms.Padding(2, 2, 2, 2);
            this.diastolicTB.Name = "diastolicTB";
            this.diastolicTB.Size = new System.Drawing.Size(139, 20);
            this.diastolicTB.TabIndex = 12;
            // 
            // systolicTB
            // 
            this.systolicTB.Location = new System.Drawing.Point(136, 71);
            this.systolicTB.Margin = new System.Windows.Forms.Padding(2, 2, 2, 2);
            this.systolicTB.Name = "systolicTB";
            this.systolicTB.Size = new System.Drawing.Size(139, 20);
            this.systolicTB.TabIndex = 11;
            // 
            // tempTB
            // 
            this.tempTB.Location = new System.Drawing.Point(136, 97);
            this.tempTB.Margin = new System.Windows.Forms.Padding(2, 2, 2, 2);
            this.tempTB.Name = "tempTB";
            this.tempTB.Size = new System.Drawing.Size(139, 20);
            this.tempTB.TabIndex = 10;
            // 
            // diastolicLbl
            // 
            this.diastolicLbl.AutoSize = true;
            this.diastolicLbl.Location = new System.Drawing.Point(83, 42);
            this.diastolicLbl.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.diastolicLbl.Name = "diastolicLbl";
            this.diastolicLbl.Size = new System.Drawing.Size(50, 13);
            this.diastolicLbl.TabIndex = 9;
            this.diastolicLbl.Text = "Diastolic:";
            // 
            // systolicLbl
            // 
            this.systolicLbl.AutoSize = true;
            this.systolicLbl.Location = new System.Drawing.Point(87, 71);
            this.systolicLbl.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.systolicLbl.Name = "systolicLbl";
            this.systolicLbl.Size = new System.Drawing.Size(46, 13);
            this.systolicLbl.TabIndex = 8;
            this.systolicLbl.Text = "Systolic:";
            // 
            // tempLbl
            // 
            this.tempLbl.AutoSize = true;
            this.tempLbl.Location = new System.Drawing.Point(63, 97);
            this.tempLbl.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.tempLbl.Name = "tempLbl";
            this.tempLbl.Size = new System.Drawing.Size(70, 13);
            this.tempLbl.TabIndex = 7;
            this.tempLbl.Text = "Temperature:";
            // 
            // pulseLbl
            // 
            this.pulseLbl.AutoSize = true;
            this.pulseLbl.Location = new System.Drawing.Point(97, 125);
            this.pulseLbl.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.pulseLbl.Name = "pulseLbl";
            this.pulseLbl.Size = new System.Drawing.Size(36, 13);
            this.pulseLbl.TabIndex = 6;
            this.pulseLbl.Text = "Pulse:";
            // 
            // diagnosisLbl
            // 
            this.diagnosisLbl.AutoSize = true;
            this.diagnosisLbl.Location = new System.Drawing.Point(76, 149);
            this.diagnosisLbl.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.diagnosisLbl.Name = "diagnosisLbl";
            this.diagnosisLbl.Size = new System.Drawing.Size(56, 13);
            this.diagnosisLbl.TabIndex = 5;
            this.diagnosisLbl.Text = "Diagnosis:";
            // 
            // testLbl
            // 
            this.testLbl.AutoSize = true;
            this.testLbl.Location = new System.Drawing.Point(90, 172);
            this.testLbl.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.testLbl.Name = "testLbl";
            this.testLbl.Size = new System.Drawing.Size(42, 13);
            this.testLbl.TabIndex = 4;
            this.testLbl.Text = "Test(s):";
            // 
            // hepBCB
            // 
            this.hepBCB.AutoSize = true;
            this.hepBCB.Location = new System.Drawing.Point(136, 246);
            this.hepBCB.Margin = new System.Windows.Forms.Padding(2, 2, 2, 2);
            this.hepBCB.Name = "hepBCB";
            this.hepBCB.Size = new System.Drawing.Size(77, 17);
            this.hepBCB.TabIndex = 3;
            this.hepBCB.Text = "Hepatitis B";
            this.hepBCB.UseVisualStyleBackColor = true;
            // 
            // hepACB
            // 
            this.hepACB.AutoSize = true;
            this.hepACB.Location = new System.Drawing.Point(136, 229);
            this.hepACB.Margin = new System.Windows.Forms.Padding(2, 2, 2, 2);
            this.hepACB.Name = "hepACB";
            this.hepACB.Size = new System.Drawing.Size(77, 17);
            this.hepACB.TabIndex = 2;
            this.hepACB.Text = "Hepatitis A";
            this.hepACB.UseVisualStyleBackColor = true;
            // 
            // ldlCB
            // 
            this.ldlCB.AutoSize = true;
            this.ldlCB.Location = new System.Drawing.Point(136, 210);
            this.ldlCB.Margin = new System.Windows.Forms.Padding(2, 2, 2, 2);
            this.ldlCB.Name = "ldlCB";
            this.ldlCB.Size = new System.Drawing.Size(149, 17);
            this.ldlCB.TabIndex = 1;
            this.ldlCB.Text = "Low Densisty Lipoproteins";
            this.ldlCB.UseVisualStyleBackColor = true;
            // 
            // whitebloodcellCB
            // 
            this.whitebloodcellCB.AutoSize = true;
            this.whitebloodcellCB.Location = new System.Drawing.Point(136, 190);
            this.whitebloodcellCB.Margin = new System.Windows.Forms.Padding(2, 2, 2, 2);
            this.whitebloodcellCB.Name = "whitebloodcellCB";
            this.whitebloodcellCB.Size = new System.Drawing.Size(104, 17);
            this.whitebloodcellCB.TabIndex = 0;
            this.whitebloodcellCB.Text = "White Blood Cell";
            this.whitebloodcellCB.UseVisualStyleBackColor = true;
            // 
            // patientChecksDGV
            // 
            this.patientChecksDGV.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.patientChecksDGV.Location = new System.Drawing.Point(4, 4);
            this.patientChecksDGV.Margin = new System.Windows.Forms.Padding(2, 2, 2, 2);
            this.patientChecksDGV.Name = "patientChecksDGV";
            this.patientChecksDGV.RowTemplate.Height = 28;
            this.patientChecksDGV.Size = new System.Drawing.Size(701, 369);
            this.patientChecksDGV.TabIndex = 1;
            this.patientChecksDGV.CellClick += new System.Windows.Forms.DataGridViewCellEventHandler(this.patientChecksDGV_CellClick);
            this.patientChecksDGV.DataBindingComplete += new System.Windows.Forms.DataGridViewBindingCompleteEventHandler(this.patientChecksDGV_DataBindingComplete);
            // 
            // updatesTab
            // 
            this.updatesTab.Controls.Add(this.updateTestAndDiagnosisPanel);
            this.updatesTab.Location = new System.Drawing.Point(4, 22);
            this.updatesTab.Name = "updatesTab";
            this.updatesTab.Padding = new System.Windows.Forms.Padding(3, 3, 3, 3);
            this.updatesTab.Size = new System.Drawing.Size(707, 415);
            this.updatesTab.TabIndex = 3;
            this.updatesTab.Text = "Updates";
            this.updatesTab.UseVisualStyleBackColor = true;
            this.updatesTab.Layout += new System.Windows.Forms.LayoutEventHandler(this.updatesTab_Layout);
            // 
            // updateTestAndDiagnosisPanel
            // 
            this.updateTestAndDiagnosisPanel.Controls.Add(this.performedOnLbl);
            this.updateTestAndDiagnosisPanel.Controls.Add(this.performedOnDTP);
            this.updateTestAndDiagnosisPanel.Controls.Add(this.testTypeListBox);
            this.updateTestAndDiagnosisPanel.Controls.Add(this.testTypeLbl);
            this.updateTestAndDiagnosisPanel.Controls.Add(this.updateErrLbl);
            this.updateTestAndDiagnosisPanel.Controls.Add(this.cancelUpdateBtn);
            this.updateTestAndDiagnosisPanel.Controls.Add(this.updateTestBtn);
            this.updateTestAndDiagnosisPanel.Controls.Add(this.updatedDiagnosisLbl);
            this.updateTestAndDiagnosisPanel.Controls.Add(this.updatedDiagnosisTB);
            this.updateTestAndDiagnosisPanel.Controls.Add(this.testResultLbl);
            this.updateTestAndDiagnosisPanel.Controls.Add(this.diagnosisIdLbl);
            this.updateTestAndDiagnosisPanel.Controls.Add(this.diagnosisIdTB);
            this.updateTestAndDiagnosisPanel.Controls.Add(this.testResultTB);
            this.updateTestAndDiagnosisPanel.Controls.Add(this.updateVisitInfo);
            this.updateTestAndDiagnosisPanel.Location = new System.Drawing.Point(5, 5);
            this.updateTestAndDiagnosisPanel.Margin = new System.Windows.Forms.Padding(2, 2, 2, 2);
            this.updateTestAndDiagnosisPanel.Name = "updateTestAndDiagnosisPanel";
            this.updateTestAndDiagnosisPanel.Size = new System.Drawing.Size(413, 409);
            this.updateTestAndDiagnosisPanel.TabIndex = 0;
            // 
            // performedOnLbl
            // 
            this.performedOnLbl.AutoSize = true;
            this.performedOnLbl.Location = new System.Drawing.Point(42, 163);
            this.performedOnLbl.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.performedOnLbl.Name = "performedOnLbl";
            this.performedOnLbl.Size = new System.Drawing.Size(58, 13);
            this.performedOnLbl.TabIndex = 19;
            this.performedOnLbl.Text = "Performed:";
            // 
            // performedOnDTP
            // 
            this.performedOnDTP.Location = new System.Drawing.Point(104, 159);
            this.performedOnDTP.Margin = new System.Windows.Forms.Padding(2, 2, 2, 2);
            this.performedOnDTP.Name = "performedOnDTP";
            this.performedOnDTP.Size = new System.Drawing.Size(198, 20);
            this.performedOnDTP.TabIndex = 1;
            // 
            // testTypeListBox
            // 
            this.testTypeListBox.FormattingEnabled = true;
            this.testTypeListBox.Location = new System.Drawing.Point(104, 89);
            this.testTypeListBox.Margin = new System.Windows.Forms.Padding(2, 2, 2, 2);
            this.testTypeListBox.Name = "testTypeListBox";
            this.testTypeListBox.Size = new System.Drawing.Size(198, 56);
            this.testTypeListBox.TabIndex = 18;
            // 
            // testTypeLbl
            // 
            this.testTypeLbl.AutoSize = true;
            this.testTypeLbl.Location = new System.Drawing.Point(45, 92);
            this.testTypeLbl.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.testTypeLbl.Name = "testTypeLbl";
            this.testTypeLbl.Size = new System.Drawing.Size(58, 13);
            this.testTypeLbl.TabIndex = 17;
            this.testTypeLbl.Text = "Test Type:";
            // 
            // updateErrLbl
            // 
            this.updateErrLbl.AutoSize = true;
            this.updateErrLbl.Location = new System.Drawing.Point(65, 230);
            this.updateErrLbl.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.updateErrLbl.Name = "updateErrLbl";
            this.updateErrLbl.Size = new System.Drawing.Size(0, 13);
            this.updateErrLbl.TabIndex = 16;
            // 
            // cancelUpdateBtn
            // 
            this.cancelUpdateBtn.Location = new System.Drawing.Point(151, 269);
            this.cancelUpdateBtn.Margin = new System.Windows.Forms.Padding(2, 2, 2, 2);
            this.cancelUpdateBtn.Name = "cancelUpdateBtn";
            this.cancelUpdateBtn.Size = new System.Drawing.Size(50, 23);
            this.cancelUpdateBtn.TabIndex = 15;
            this.cancelUpdateBtn.Text = "Cancel";
            this.cancelUpdateBtn.UseVisualStyleBackColor = true;
            this.cancelUpdateBtn.Click += new System.EventHandler(this.cancelUpdateBtn_Click);
            // 
            // updateTestBtn
            // 
            this.updateTestBtn.Location = new System.Drawing.Point(87, 269);
            this.updateTestBtn.Margin = new System.Windows.Forms.Padding(2, 2, 2, 2);
            this.updateTestBtn.Name = "updateTestBtn";
            this.updateTestBtn.Size = new System.Drawing.Size(50, 23);
            this.updateTestBtn.TabIndex = 14;
            this.updateTestBtn.Text = "Update";
            this.updateTestBtn.UseVisualStyleBackColor = true;
            this.updateTestBtn.Click += new System.EventHandler(this.updateTestBtn_Click);
            // 
            // updatedDiagnosisLbl
            // 
            this.updatedDiagnosisLbl.AutoSize = true;
            this.updatedDiagnosisLbl.Location = new System.Drawing.Point(19, 237);
            this.updatedDiagnosisLbl.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.updatedDiagnosisLbl.Name = "updatedDiagnosisLbl";
            this.updatedDiagnosisLbl.Size = new System.Drawing.Size(81, 13);
            this.updatedDiagnosisLbl.TabIndex = 13;
            this.updatedDiagnosisLbl.Text = "Final Diagnosis:";
            // 
            // updatedDiagnosisTB
            // 
            this.updatedDiagnosisTB.Location = new System.Drawing.Point(104, 233);
            this.updatedDiagnosisTB.Margin = new System.Windows.Forms.Padding(2, 2, 2, 2);
            this.updatedDiagnosisTB.Name = "updatedDiagnosisTB";
            this.updatedDiagnosisTB.Size = new System.Drawing.Size(115, 20);
            this.updatedDiagnosisTB.TabIndex = 12;
            // 
            // testResultLbl
            // 
            this.testResultLbl.AutoSize = true;
            this.testResultLbl.Location = new System.Drawing.Point(37, 201);
            this.testResultLbl.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.testResultLbl.Name = "testResultLbl";
            this.testResultLbl.Size = new System.Drawing.Size(64, 13);
            this.testResultLbl.TabIndex = 11;
            this.testResultLbl.Text = "Test Result:";
            // 
            // diagnosisIdLbl
            // 
            this.diagnosisIdLbl.AutoSize = true;
            this.diagnosisIdLbl.Location = new System.Drawing.Point(31, 61);
            this.diagnosisIdLbl.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.diagnosisIdLbl.Name = "diagnosisIdLbl";
            this.diagnosisIdLbl.Size = new System.Drawing.Size(70, 13);
            this.diagnosisIdLbl.TabIndex = 10;
            this.diagnosisIdLbl.Text = "Diagnosis ID:";
            // 
            // diagnosisIdTB
            // 
            this.diagnosisIdTB.Location = new System.Drawing.Point(104, 57);
            this.diagnosisIdTB.Margin = new System.Windows.Forms.Padding(2, 2, 2, 2);
            this.diagnosisIdTB.Name = "diagnosisIdTB";
            this.diagnosisIdTB.Size = new System.Drawing.Size(60, 20);
            this.diagnosisIdTB.TabIndex = 9;
            // 
            // testResultTB
            // 
            this.testResultTB.Location = new System.Drawing.Point(104, 197);
            this.testResultTB.Margin = new System.Windows.Forms.Padding(2, 2, 2, 2);
            this.testResultTB.Name = "testResultTB";
            this.testResultTB.Size = new System.Drawing.Size(115, 20);
            this.testResultTB.TabIndex = 2;
            // 
            // updateVisitInfo
            // 
            this.updateVisitInfo.AutoSize = true;
            this.updateVisitInfo.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.updateVisitInfo.Location = new System.Drawing.Point(15, 10);
            this.updateVisitInfo.Name = "updateVisitInfo";
            this.updateVisitInfo.Size = new System.Drawing.Size(202, 20);
            this.updateVisitInfo.TabIndex = 1;
            this.updateVisitInfo.Text = "Update Test and Diagnosis";
            // 
            // currentUserLbl
            // 
            this.currentUserLbl.AutoSize = true;
            this.currentUserLbl.Location = new System.Drawing.Point(558, 9);
            this.currentUserLbl.Name = "currentUserLbl";
            this.currentUserLbl.Size = new System.Drawing.Size(106, 13);
            this.currentUserLbl.TabIndex = 30;
            this.currentUserLbl.Text = "Welcome, Username";
            // 
            // logoutBtn
            // 
            this.logoutBtn.Location = new System.Drawing.Point(683, 4);
            this.logoutBtn.Name = "logoutBtn";
            this.logoutBtn.Size = new System.Drawing.Size(53, 23);
            this.logoutBtn.TabIndex = 31;
            this.logoutBtn.Text = "Log Out";
            this.logoutBtn.UseVisualStyleBackColor = true;
            this.logoutBtn.Click += new System.EventHandler(this.logoutBtn_Click);
            // 
            // adminAppPanel
            // 
            this.adminAppPanel.Controls.Add(this.enterSQLBtn);
            this.adminAppPanel.Controls.Add(this.adminSQLDGV);
            this.adminAppPanel.Controls.Add(this.adminSQLRTB);
            this.adminAppPanel.Controls.Add(this.adminAppLbl);
            this.adminAppPanel.Location = new System.Drawing.Point(-5, -7);
            this.adminAppPanel.Name = "adminAppPanel";
            this.adminAppPanel.Size = new System.Drawing.Size(715, 411);
            this.adminAppPanel.TabIndex = 32;
            // 
            // enterSQLBtn
            // 
            this.enterSQLBtn.Location = new System.Drawing.Point(624, 384);
            this.enterSQLBtn.Name = "enterSQLBtn";
            this.enterSQLBtn.Size = new System.Drawing.Size(75, 23);
            this.enterSQLBtn.TabIndex = 3;
            this.enterSQLBtn.Text = "Go";
            this.enterSQLBtn.UseVisualStyleBackColor = true;
            this.enterSQLBtn.Click += new System.EventHandler(this.enterSQLBtn_Click);
            // 
            // adminSQLDGV
            // 
            this.adminSQLDGV.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.adminSQLDGV.Location = new System.Drawing.Point(10, 244);
            this.adminSQLDGV.Name = "adminSQLDGV";
            this.adminSQLDGV.Size = new System.Drawing.Size(689, 134);
            this.adminSQLDGV.TabIndex = 2;
            // 
            // adminSQLRTB
            // 
            this.adminSQLRTB.Location = new System.Drawing.Point(11, 52);
            this.adminSQLRTB.Name = "adminSQLRTB";
            this.adminSQLRTB.Size = new System.Drawing.Size(688, 186);
            this.adminSQLRTB.TabIndex = 1;
            this.adminSQLRTB.Text = "";
            // 
            // adminAppLbl
            // 
            this.adminAppLbl.AutoSize = true;
            this.adminAppLbl.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.adminAppLbl.Location = new System.Drawing.Point(7, 26);
            this.adminAppLbl.Name = "adminAppLbl";
            this.adminAppLbl.Size = new System.Drawing.Size(171, 20);
            this.adminAppLbl.TabIndex = 0;
            this.adminAppLbl.Text = "Enter SQL Statements";
            // 
            // adminTabControl
            // 
            this.adminTabControl.Controls.Add(this.queryTabPage);
            this.adminTabControl.Controls.Add(this.datePickerTabPage);
            this.adminTabControl.Location = new System.Drawing.Point(12, 13);
            this.adminTabControl.Name = "adminTabControl";
            this.adminTabControl.SelectedIndex = 0;
            this.adminTabControl.Size = new System.Drawing.Size(723, 445);
            this.adminTabControl.TabIndex = 33;
            // 
            // queryTabPage
            // 
            this.queryTabPage.Controls.Add(this.adminAppPanel);
            this.queryTabPage.Location = new System.Drawing.Point(4, 22);
            this.queryTabPage.Name = "queryTabPage";
            this.queryTabPage.Padding = new System.Windows.Forms.Padding(3);
            this.queryTabPage.Size = new System.Drawing.Size(715, 419);
            this.queryTabPage.TabIndex = 0;
            this.queryTabPage.Text = "Query";
            this.queryTabPage.UseVisualStyleBackColor = true;
            // 
            // datePickerTabPage
            // 
            this.datePickerTabPage.Controls.Add(this.dateVisitDGV);
            this.datePickerTabPage.Controls.Add(this.betweenDatesBtn);
            this.datePickerTabPage.Controls.Add(this.endDateLbl);
            this.datePickerTabPage.Controls.Add(this.startDateLbl);
            this.datePickerTabPage.Controls.Add(this.endDateTimePicker);
            this.datePickerTabPage.Controls.Add(this.startDateTimePicker);
            this.datePickerTabPage.Location = new System.Drawing.Point(4, 22);
            this.datePickerTabPage.Name = "datePickerTabPage";
            this.datePickerTabPage.Padding = new System.Windows.Forms.Padding(3);
            this.datePickerTabPage.Size = new System.Drawing.Size(715, 419);
            this.datePickerTabPage.TabIndex = 1;
            this.datePickerTabPage.Text = "Visit Info";
            this.datePickerTabPage.UseVisualStyleBackColor = true;
            // 
            // startDateTimePicker
            // 
            this.startDateTimePicker.Location = new System.Drawing.Point(93, 64);
            this.startDateTimePicker.Name = "startDateTimePicker";
            this.startDateTimePicker.Size = new System.Drawing.Size(200, 20);
            this.startDateTimePicker.TabIndex = 0;
            // 
            // endDateTimePicker
            // 
            this.endDateTimePicker.Location = new System.Drawing.Point(454, 64);
            this.endDateTimePicker.Name = "endDateTimePicker";
            this.endDateTimePicker.Size = new System.Drawing.Size(200, 20);
            this.endDateTimePicker.TabIndex = 1;
            // 
            // startDateLbl
            // 
            this.startDateLbl.AutoSize = true;
            this.startDateLbl.Location = new System.Drawing.Point(61, 44);
            this.startDateLbl.Name = "startDateLbl";
            this.startDateLbl.Size = new System.Drawing.Size(55, 13);
            this.startDateLbl.TabIndex = 2;
            this.startDateLbl.Text = "Start Date";
            // 
            // endDateLbl
            // 
            this.endDateLbl.AutoSize = true;
            this.endDateLbl.Location = new System.Drawing.Point(436, 44);
            this.endDateLbl.Name = "endDateLbl";
            this.endDateLbl.RightToLeft = System.Windows.Forms.RightToLeft.No;
            this.endDateLbl.Size = new System.Drawing.Size(52, 13);
            this.endDateLbl.TabIndex = 3;
            this.endDateLbl.Text = "End Date";
            // 
            // betweenDatesBtn
            // 
            this.betweenDatesBtn.Location = new System.Drawing.Point(337, 110);
            this.betweenDatesBtn.Name = "betweenDatesBtn";
            this.betweenDatesBtn.Size = new System.Drawing.Size(75, 23);
            this.betweenDatesBtn.TabIndex = 4;
            this.betweenDatesBtn.Text = "Go";
            this.betweenDatesBtn.UseVisualStyleBackColor = true;
            this.betweenDatesBtn.Click += new System.EventHandler(this.betweenDatesBtn_Click);
            // 
            // dateVisitDGV
            // 
            this.dateVisitDGV.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dateVisitDGV.Location = new System.Drawing.Point(40, 160);
            this.dateVisitDGV.Name = "dateVisitDGV";
            this.dateVisitDGV.Size = new System.Drawing.Size(636, 224);
            this.dateVisitDGV.TabIndex = 5;
            // 
            // HRSystemView
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.Color.White;
            this.ClientSize = new System.Drawing.Size(739, 479);
            this.Controls.Add(this.currentUserLbl);
            this.Controls.Add(this.nurseAppTabControl);
            this.Controls.Add(this.adminTabControl);
            this.Controls.Add(this.logoutBtn);
            this.Controls.Add(this.loginPanel);
            this.Controls.Add(this.regPanel);
            this.Name = "HrSystemView";
            this.Text = "Healthcare System";
            this.regPanel.ResumeLayout(false);
            this.regPanel.PerformLayout();
            this.registerGenderPanel.ResumeLayout(false);
            this.registerGenderPanel.PerformLayout();
            this.rolePanel.ResumeLayout(false);
            this.rolePanel.PerformLayout();
            this.loginPanel.ResumeLayout(false);
            this.loginPanel.PerformLayout();
            this.newPatientPanel.ResumeLayout(false);
            this.newPatientPanel.PerformLayout();
            this.nurseAppTabControl.ResumeLayout(false);
            this.patientTab.ResumeLayout(false);
            this.panel1.ResumeLayout(false);
            this.panel1.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.searchPatientDGV)).EndInit();
            this.appointmentTab.ResumeLayout(false);
            this.panel2.ResumeLayout(false);
            this.panel2.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.searchVisitInfoDGV)).EndInit();
            this.appointmentPanel.ResumeLayout(false);
            this.appointmentPanel.PerformLayout();
            this.checksTab.ResumeLayout(false);
            this.performChecksPanel.ResumeLayout(false);
            this.performChecksPanel.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.patientChecksDGV)).EndInit();
            this.updatesTab.ResumeLayout(false);
            this.updateTestAndDiagnosisPanel.ResumeLayout(false);
            this.updateTestAndDiagnosisPanel.PerformLayout();
            this.adminAppPanel.ResumeLayout(false);
            this.adminAppPanel.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.adminSQLDGV)).EndInit();
            this.adminTabControl.ResumeLayout(false);
            this.queryTabPage.ResumeLayout(false);
            this.datePickerTabPage.ResumeLayout(false);
            this.datePickerTabPage.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dateVisitDGV)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.TextBox usernameTB;
        private System.Windows.Forms.TextBox passwordTB;
        private System.Windows.Forms.Label usernameLbl;
        private System.Windows.Forms.Label passwordLbl;
        private System.Windows.Forms.Button loginBtn;
        private System.Windows.Forms.Button registerBtn;
        private System.Windows.Forms.TextBox phoneTB;
        private System.Windows.Forms.TextBox fnameTB;
        private System.Windows.Forms.TextBox lnameTB;
        private System.Windows.Forms.Label fnameLbl;
        private System.Windows.Forms.Label lnameLbl;
        private System.Windows.Forms.Label birthdateLbl;
        private System.Windows.Forms.Label phoneLbl;
        private System.Windows.Forms.RadioButton nurseRB;
        private System.Windows.Forms.RadioButton adminRB;
        private System.Windows.Forms.DateTimePicker birthDateDTP;
        private System.Windows.Forms.Label regUsernameLbl;
        private System.Windows.Forms.Label regPasswordLbl;
        private System.Windows.Forms.TextBox regPasswordTB;
        private System.Windows.Forms.TextBox regUsernameTB;
        private System.Windows.Forms.Button cancelBtn;
        private System.Windows.Forms.Button regRegisterBtn;
        private System.Windows.Forms.Label regErrorLbl;
        private System.Windows.Forms.Panel regPanel;
        private System.Windows.Forms.Panel loginPanel;
        private System.Windows.Forms.Panel newPatientPanel;
        private System.Windows.Forms.Label patientFNameLabel;
        private System.Windows.Forms.TextBox patientFNameTextBox;
        private System.Windows.Forms.Label newPatientLabel;
        private System.Windows.Forms.TextBox patientLNameTextBox;
        private System.Windows.Forms.Label patientLNameLabel;
        private System.Windows.Forms.DateTimePicker newPatientDateTimePicker;
        private System.Windows.Forms.Label patientBirthdateLabel;
        private System.Windows.Forms.Label patientPhoneLabel;
        private System.Windows.Forms.TextBox patientPhoneTextBox;
        private System.Windows.Forms.TextBox patientStreetTextbox;
        private System.Windows.Forms.Label patientAddressLabel;
        private System.Windows.Forms.Label patientErrorLabel;
        private System.Windows.Forms.Button newPatientButton;
        private System.Windows.Forms.RadioButton patientFemaleRB;
        private System.Windows.Forms.RadioButton patientMaleRB;
        private System.Windows.Forms.Label patientGenderLbl;
        private System.Windows.Forms.Panel rolePanel;
        private System.Windows.Forms.Panel registerGenderPanel;
        private System.Windows.Forms.RadioButton registerMaleRB;
        private System.Windows.Forms.RadioButton registerFemaleRB;
        private System.Windows.Forms.Label registerGenderLbl;
        private System.Windows.Forms.Label registerRoleLbl;
        private System.Windows.Forms.TabControl nurseAppTabControl;
        private System.Windows.Forms.TabPage patientTab;
        private System.Windows.Forms.TabPage appointmentTab;
        private System.Windows.Forms.Button apptCancelBtn;
        private System.Windows.Forms.Button apptAddBtn;
        private System.Windows.Forms.Panel appointmentPanel;
        private System.Windows.Forms.ListBox doctorListBox;
        private System.Windows.Forms.Label addAppointmentLbl;
        private System.Windows.Forms.DateTimePicker apptDTP;
        private System.Windows.Forms.Label apptDateLbl;
        private System.Windows.Forms.Label apptDoctorLbl;
        private System.Windows.Forms.Label patientListLbl;
        private System.Windows.Forms.ListBox patientListBox;
        private System.Windows.Forms.RichTextBox apptReasonRTB;
        private System.Windows.Forms.Label apptReasonLbl;
        private System.Windows.Forms.Label streetLabel;
        private System.Windows.Forms.Label patientCityLabel;
        private System.Windows.Forms.Label patientAddressLine2Label;
        private System.Windows.Forms.Label patientAddress2TextBoc;
        private System.Windows.Forms.TextBox patientZipCodeTextBox;
        private System.Windows.Forms.TextBox patientCityTextBox;
        private System.Windows.Forms.TextBox patientAddress2TextBox;
        private System.Windows.Forms.Label patientZipCodeLabel;
        private System.Windows.Forms.Label patientStateLabel;
        private System.Windows.Forms.Label registerZipCodeLabel;
        private System.Windows.Forms.Label registerStateLabel;
        private System.Windows.Forms.Label registerCityLabel;
        private System.Windows.Forms.Label registerLine2Label;
        private System.Windows.Forms.TextBox registerZipCodeTextBox;
        private System.Windows.Forms.TextBox registerCityTextBox;
        private System.Windows.Forms.TextBox registerLine2TextBox;
        private System.Windows.Forms.Label registerLine1Label;
        private System.Windows.Forms.TextBox registerLine1TextBox;
        private System.Windows.Forms.Label registerAddressLabel;
        private System.Windows.Forms.Label currentUserLbl;
        private System.Windows.Forms.Button logoutBtn;
        private System.Windows.Forms.Panel adminAppPanel;
        private System.Windows.Forms.Button enterSQLBtn;
        private System.Windows.Forms.DataGridView adminSQLDGV;
        private System.Windows.Forms.RichTextBox adminSQLRTB;
        private System.Windows.Forms.Label adminAppLbl;
        private System.Windows.Forms.Panel panel1;
        private System.Windows.Forms.DataGridView searchPatientDGV;
        private System.Windows.Forms.Button searchPatientBtn;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.Label label8;
        private System.Windows.Forms.DateTimePicker searchPatientDOBDTP;
        private System.Windows.Forms.Label searchPatientDOBLbl;
        private System.Windows.Forms.TextBox searchPatientLnameTB;
        private System.Windows.Forms.Label searchPatientLnameLbl;
        private System.Windows.Forms.Label searchPatientFnameLbl;
        private System.Windows.Forms.TextBox searchPatientFnameTB;
        private System.Windows.Forms.Label searchPatientLbl;
        private System.Windows.Forms.ComboBox patientStateComboBox;
        private System.Windows.Forms.ComboBox registerStateComboBox;
        private System.Windows.Forms.TabPage checksTab;
        private System.Windows.Forms.DataGridView patientChecksDGV;
        private System.Windows.Forms.Panel performChecksPanel;
        private System.Windows.Forms.Label testLbl;
        private System.Windows.Forms.CheckBox hepBCB;
        private System.Windows.Forms.CheckBox hepACB;
        private System.Windows.Forms.CheckBox ldlCB;
        private System.Windows.Forms.CheckBox whitebloodcellCB;
        private System.Windows.Forms.TextBox systolicTB;
        private System.Windows.Forms.TextBox tempTB;
        private System.Windows.Forms.Label diastolicLbl;
        private System.Windows.Forms.Label systolicLbl;
        private System.Windows.Forms.Label tempLbl;
        private System.Windows.Forms.Label pulseLbl;
        private System.Windows.Forms.Label diagnosisLbl;
        private System.Windows.Forms.Button checkCancelBtn;
        private System.Windows.Forms.Button checkSaveBtn;
        private System.Windows.Forms.TextBox diagnosisTB;
        private System.Windows.Forms.TextBox pulseTB;
        private System.Windows.Forms.TextBox diastolicTB;
        private System.Windows.Forms.TabPage updatesTab;
        private System.Windows.Forms.Label checksValidationLabel;
        private System.Windows.Forms.Panel panel2;
        private System.Windows.Forms.Button searchVisitInfoBtn;
        private System.Windows.Forms.DataGridView searchVisitInfoDGV;
        private System.Windows.Forms.TextBox searchVisitInfoTB;
        private System.Windows.Forms.Label searchVisitInfoLbl;
        private System.Windows.Forms.Label searchVisitLbl;
        private System.Windows.Forms.Label searchVisitInfoErrLbL;
        private System.Windows.Forms.Panel updateTestAndDiagnosisPanel;
        private System.Windows.Forms.Label updateVisitInfo;
        private System.Windows.Forms.Label testResultLbl;
        private System.Windows.Forms.Label diagnosisIdLbl;
        private System.Windows.Forms.TextBox diagnosisIdTB;
        private System.Windows.Forms.TextBox testResultTB;
        private System.Windows.Forms.Label updatedDiagnosisLbl;
        private System.Windows.Forms.TextBox updatedDiagnosisTB;
        private System.Windows.Forms.Label updateErrLbl;
        private System.Windows.Forms.Button cancelUpdateBtn;
        private System.Windows.Forms.Button updateTestBtn;
        private System.Windows.Forms.Label testTypeLbl;
        private System.Windows.Forms.ListBox testTypeListBox;
        private System.Windows.Forms.Label performedOnLbl;
        private System.Windows.Forms.DateTimePicker performedOnDTP;
        private System.Windows.Forms.TabControl adminTabControl;
        private System.Windows.Forms.TabPage queryTabPage;
        private System.Windows.Forms.TabPage datePickerTabPage;
        private System.Windows.Forms.DataGridView dateVisitDGV;
        private System.Windows.Forms.Button betweenDatesBtn;
        private System.Windows.Forms.Label endDateLbl;
        private System.Windows.Forms.Label startDateLbl;
        private System.Windows.Forms.DateTimePicker endDateTimePicker;
        private System.Windows.Forms.DateTimePicker startDateTimePicker;
    }
}

