﻿using System;
using System.Data;
using System.Windows.Forms;
using HealthcareSystem.Controller;
using HealthcareSystem.Model;

namespace HealthcareSystem.View
{
    /// <summary>
    /// The form
    /// </summary>
    /// <seealso cref="System.Windows.Forms.Form" />
    public partial class HrSystemView : Form
    {
        private RepositoryController controller;
        private Schedule scheduled;

        /// <summary>
        /// Initializes a new instance of the class.
        /// </summary>
        /// <param name="controller">The controller.</param>
        public HrSystemView(RepositoryController controller)
        {
            this.controller = controller;
            this.InitializeComponent();
            this.regPanel.Hide();
            this.nurseAppTabControl.Hide();
            this.patientMaleRB.Checked = true;
            this.nurseRB.Checked = true;
            this.registerMaleRB.Checked = true;
            this.logoutBtn.Hide();
            this.currentUserLbl.Text = "";
            this.patientStateComboBox.Items.AddRange(this.populateStateDropdown());
            this.registerStateComboBox.Items.AddRange(this.populateStateDropdown());
            this.adminTabControl.Hide();
            this.performChecksPanel.Hide();
            this.completeChecksBtnSetup();
            this.patientChecksDGV.RowHeadersVisible = false;
            this.searchPatientDGV.RowHeadersVisible = false;
        }

        private void loginBtn_Click(object sender, EventArgs e)
        {
            if (this.controller.ValidateLogin(this.usernameTB.Text, this.passwordTB.Text))
            {
                this.currentUserLbl.Text = @"Welcome, " + this.controller.CurrentUser.Username;
                this.loginPanel.Hide();
                this.logoutBtn.Show();
                if (this.controller.CurrentUser.Type == PersonType.Administrator)
                {
                    this.adminTabControl.Show();
                }
                else
                {
                    this.nurseAppTabControl.Show();
                }                
            }
        }

        private void registerBtn_Click(object sender, EventArgs e)
        {
            this.loginPanel.Hide();
            this.regPanel.Show();
        }

        private void cancelBtn_Click(object sender, EventArgs e)
        {
            this.regPanel.Hide();
            this.loginPanel.Show();
        }

        private void regRegisterBtn_Click(object sender, EventArgs e)
        {
            char gender = (this.registerMaleRB.Checked) ? 'M' : 'F';

            string fname = this.fnameTB.Text;
            string lname = this.lnameTB.Text;
            string street = this.registerLine1TextBox.Text;
            string address2 = this.registerLine2TextBox.Text;
            string city = this.registerCityTextBox.Text;
            string state = this.registerStateComboBox.SelectedText;
            string zip = this.registerZipCodeTextBox.Text;
            string phone = this.phoneTB.Text;

            if (this.validateEntries(this.regErrorLbl, fname, lname, street, city, state, zip, phone))
            {
                if (!string.IsNullOrEmpty(this.regPasswordTB.Text) &&
                    !string.IsNullOrEmpty(this.regUsernameTB.Text))
                {
                    this.controller.RegisterUser(fname, lname, this.birthDateDTP.Value, street, address2, city, state,
                        zip, phone, gender, this.regUsernameTB.Text, this.regPasswordTB.Text, this.adminRB.Checked);
                    this.regPanel.Hide();
                    this.loginPanel.Show();
                }
            }
        }

        private void newPatientButton_Click(object sender, EventArgs e)
        {
            string fname = this.patientFNameTextBox.Text;
            string lname = this.patientLNameTextBox.Text;
            DateTime dob = this.newPatientDateTimePicker.Value;

            string street = this.patientStreetTextbox.Text;

            string address2 = null;

            if (!string.IsNullOrWhiteSpace(this.patientAddress2TextBox.Text))
            {
                address2 = this.patientAddress2TextBox.Text;
            }

            string city = this.patientCityTextBox.Text;
            string state = this.patientStateComboBox.SelectedText;
            string zip = this.patientZipCodeTextBox.Text;

            string phone = this.patientPhoneTextBox.Text;

            char gender = (this.patientMaleRB.Checked) ? 'M' : 'F';

            if (this.validateEntries(this.patientErrorLabel, fname, lname, street, city, state, zip, phone))
            {
                this.controller.AddPatient(fname, lname, dob, street, address2, city, state, zip, phone, gender);
                this.patientErrorLabel.Text = @"Patient added successfully";
                this.resetPatientFields();
            }                
                                      
        }

        private void resetPatientFields()
        {
            this.patientMaleRB.Checked = true;
            this.patientStreetTextbox.Clear();
            this.patientFNameTextBox.Clear();
            this.patientLNameTextBox.Clear();
            this.patientPhoneTextBox.Clear();
            this.newPatientDateTimePicker.Value = DateTime.Today;
        }

        private void appointmentTab_Click(object sender, EventArgs e)
        {            
            this.doctorListBox.DataSource = this.controller.GetDoctors();
            this.patientListBox.DataSource = this.controller.GetPatients();
        }

        private void doctorListBox_Format(object sender, ListControlConvertEventArgs e)
        {
            string lname = ((Doctor)e.ListItem).Lname;
            string fname = ((Doctor)e.ListItem).Fname;
            e.Value = lname + ", " + fname;
        }

        private void patientListBox_Format(object sender, ListControlConvertEventArgs e)
        {
            string lname = ((Patient)e.ListItem).Lname;
            string fname = ((Patient)e.ListItem).Fname;
            e.Value = lname + ", " + fname;
        }

        private void apptAddBtn_Click(object sender, EventArgs e)
        {
            this.controller.AddSchedule((Doctor) this.doctorListBox.SelectedItem, this.apptDTP.Value,
                (Patient) this.patientListBox.SelectedItem, this.apptReasonRTB.Text);
            this.doctorListBox.ClearSelected();
            this.patientListBox.ClearSelected();
            this.apptDTP.Value = DateTime.Now;
            this.apptReasonRTB.Text = "";
        }

        private void logoutBtn_Click(object sender, EventArgs e)
        {
            this.nurseAppTabControl.Hide();
            this.currentUserLbl.Text = "";
            this.usernameTB.Text = "";
            this.passwordTB.Text = "";
            this.logoutBtn.Hide();
            this.adminSQLDGV.Columns.Clear();
            this.dateVisitDGV.Columns.Clear();
            this.startDateTimePicker.Value = DateTime.Today;
            this.endDateTimePicker.Value = DateTime.Today;
            this.adminTabControl.Hide();
            this.loginPanel.Show();
            
        }

        private void enterSQLBtn_Click(object sender, EventArgs e)
        {
            DataTable placeholder = new DataTable();
            
            placeholder.Columns.Add("Result");
            placeholder.Rows.Add("Operation Performed.");
            DataSet ds = this.controller.RunAdminQuery(this.adminSQLRTB.Text);
            this.adminSQLDGV.DataSource = (ds.Tables.Count > 0) ? ds.Tables[0] : placeholder;
            this.adminSQLRTB.Clear();
        }

        private void searchPatientBtn_Click(object sender, EventArgs e)
        {
            string fname = "";
            string lname = "";
            DateTime bdate = DateTime.Today;

            if (!string.IsNullOrWhiteSpace(this.searchPatientFnameTB.Text))
            {
                fname = this.searchPatientFnameTB.Text;
            }

            if (!string.IsNullOrWhiteSpace(this.searchPatientLnameTB.Text))
            {
                lname = this.searchPatientLnameTB.Text;
            }

            if (!(this.searchPatientDOBDTP.Value >= DateTime.Today))
            {
                bdate = this.searchPatientDOBDTP.Value;
            }

            DataSet patients = this.controller.SearchPatients(fname, lname, bdate);

            this.searchPatientDGV.DataSource = patients.Tables[0];

        }

        private object[] populateStateDropdown()
        {
            object[] states = {
            "AL", "AK", "AZ", "AR", "CA", "CO", "CT", "DE", "DC", "FL", "GA", "HI", "ID", "IL", "IN", "IA", "KS", "KY", "LA", "ME", "MD",
                "MA","MI","MN","MS","MO","MT","NE","NV","NH","NJ","NM","NY","NC","ND","OH","OK","OR","PA","RI","SC",
                "SD", "TN", "TX", "UT", "VT", "VA", "WA","WV","WI", "WY"
        };
        return states;
        }

        private bool validateEntries(Label warning, string fname, string lname, string address1, string city, string state, string zipcode, string phone)
        {
            if (!Utility.ValidateForm.ValidateName(fname))
            {
                warning.Text = @"Invalid first name.";
                return false;
            }

            if (!Utility.ValidateForm.ValidateName(lname))
            {
                warning.Text = @"Invalid last name";
                return false;
            }

            if (!Utility.ValidateForm.ValidateAddress(address1))
            {
                warning.Text = @"Invalid address.";
                return false;
            }

            if (!Utility.ValidateForm.ValidateAddress(city))
            {
                warning.Text = @"Invalid city.";
                return false;
            }

            if (!Utility.ValidateForm.ValidateZipCode(zipcode))
            {
                warning.Text = @"Invalid zip code.";
                return false;
            }

            if (!Utility.ValidateForm.ValidatePhoneNumber(phone))
            {
                warning.Text = @"Invalid phone number";
                return false;
            }

            if (!Utility.ValidateForm.ValidateState(state))
            {
                warning.Text = @"Please choose a state.";
                return false;
            }

            return true;
        }

        private void checksTab_Layout(object sender, LayoutEventArgs e)
        {
            this.patientChecksDGV.DataSource = this.controller.GetSchedules();
            this.patientChecksDGV.Columns["doctorID"].Visible = false;
            this.patientChecksDGV.Columns["patientID"].Visible = false;
            this.patientChecksDGV.Columns["scheduleID"].Visible = false;
        }

        private void patientChecksDGV_DataBindingComplete(object sender, DataGridViewBindingCompleteEventArgs e)
        {
            for (int i = 0; i < this.patientChecksDGV.Rows.Count; i++) {                
                DataGridViewButtonCell btn = new DataGridViewButtonCell();
                btn.Value = "Perform Checks";
                this.patientChecksDGV.Rows[i].Cells[0] = btn;
            }            
        }

        private void patientChecksDGV_CellClick(object sender, DataGridViewCellEventArgs e)
        {
            if (e.ColumnIndex >= 0 && e.RowIndex >= 0 && this.patientChecksDGV.Columns[e.ColumnIndex].Name == "")
            {
                DataGridViewRow row = (DataGridViewRow) this.patientChecksDGV.Rows[e.RowIndex];
                int doctorId = (int) row.Cells["doctorID"].Value;
                DateTime datetime = (DateTime) row.Cells["datetime"].Value;
                int patientId = (int) row.Cells["patientID"].Value;
                string reason = (string) row.Cells["reason"].Value;
                int scheduleId = (int) row.Cells["scheduleID"].Value;
                this.scheduled = new Schedule(scheduleId, doctorId, datetime, patientId, reason);
                this.performChecksPanel.Show();
                this.patientChecksDGV.Hide();
            }
        }

        private void completeChecksBtnSetup()
        {
            DataGridViewButtonColumn col = new DataGridViewButtonColumn();
            col.Name = "";
            this.patientChecksDGV.Columns.Add(col);
        }

        private void checkSaveBtn_Click(object sender, EventArgs e)
        {
            if (this.validateCheckEntry())
            {
                // Add info to db using this.scheduled object
                this.controller.AddCheck(this.scheduled, this.diastolicTB.Text, this.systolicTB.Text, this.tempTB.Text,
                    this.pulseTB.Text, this.diagnosisTB.Text, this.whitebloodcellCB.Checked, this.ldlCB.Checked,
                    this.hepACB.Checked, this.hepBCB.Checked, this.controller.CurrentUser.PersonId);
                this.performChecksPanel.Hide();
                this.patientChecksDGV.Show();
            }
        }

        private void checkCancelBtn_Click(object sender, EventArgs e)
        {
            //clear form
            this.performChecksPanel.Hide();
            this.patientChecksDGV.Show();
        }

        private bool validateCheckEntry()
        {
            if (!Utility.ValidateForm.ValidateCheckEntry(this.diastolicTB.Text))
            {
                this.checksValidationLabel.Text = @"Please provide valid diastolic pressure.";
                return false;
            }
            if (!Utility.ValidateForm.ValidateCheckEntry(this.systolicTB.Text))
            {
                this.checksValidationLabel.Text = @"Please provide valid systolic pressure.";
                return false;
            }
            if (!Utility.ValidateForm.ValidateCheckEntry(this.tempTB.Text))
            {
                this.checksValidationLabel.Text = @"Please provide a valid temperature.";
                return false;
            }

            if (!Utility.ValidateForm.ValidateCheckEntry(this.pulseTB.Text))
            {
                this.checksValidationLabel.Text = @"Please provide a valid pulse.";
                return false;
            }
            return true;
        }

        private void searchVisitInfoBtn_Click(object sender, EventArgs e)
        {
            int patientID = 0;
            this.searchVisitInfoErrLbL.Text = "";
            try
            {
                patientID = Int32.Parse(this.searchVisitInfoTB.Text);
                this.searchVisitInfoDGV.DataSource = this.controller.SearchVisitInfo(patientID);
            }
            catch
            {
                this.searchVisitInfoErrLbL.Text = @"Please input a valid number";
                throw;
            }
        }

        private void searchVisitInfoDGV_DataBindingComplete(object sender, DataGridViewBindingCompleteEventArgs e)
        {
            this.searchVisitInfoDGV.Columns["ScheduleId"].Visible = false;
            this.searchVisitInfoDGV.Columns["NurseID"].Visible = false;
            this.searchVisitInfoDGV.Columns["WhiteBloodCellTest"].Visible = false;
            this.searchVisitInfoDGV.Columns["HepATest"].Visible = false;
            this.searchVisitInfoDGV.Columns["HepBTest"].Visible = false;
            this.searchVisitInfoDGV.Columns["LdlTest"].Visible = false;
        }

        private void updateTestBtn_Click(object sender, EventArgs e)
        {
            int diagnosisID = 0;
            this.updateErrLbl.Text = "";
            try
            {
                diagnosisID = Int32.Parse(this.diagnosisIdTB.Text);
                if (((this.testTypeListBox.SelectedItem != null) && (this.performedOnDTP.Value.Date <= DateTime.Now.Date) && !String.IsNullOrEmpty(this.testResultTB.Text))
                    || !String.IsNullOrEmpty(this.updatedDiagnosisTB.Text) && (this.performedOnDTP.Value.Date <= DateTime.Now.Date))
                {
                    this.controller.UpdateTestAndDiagnosis(diagnosisID, (TestType)this.testTypeListBox.SelectedItem, this.testResultTB.Text, this.updatedDiagnosisTB.Text, this.performedOnDTP.Value);
                    this.updateErrLbl.Text = @"Successfully updated.";
                    this.diagnosisIdTB.Text = "";
                    this.testResultTB.Text = "";
                    this.updatedDiagnosisTB.Text = "";
                }
                else
                {
                    this.updateErrLbl.Text = @"Must input either final diagnosis or test results.";
                }
                
            }
            catch
            {
                this.updateErrLbl.Text = @"Action Failed. Please verify that form is filled correctly.";
                throw;
            }
        }

        private void cancelUpdateBtn_Click(object sender, EventArgs e)
        {
            this.diagnosisIdTB.Text = "";
            this.testResultTB.Text = "";
            this.updatedDiagnosisTB.Text = "";
        }

        private void updatesTab_Layout(object sender, LayoutEventArgs e)
        {
            Array types = Enum.GetValues(typeof(TestType));

            this.testTypeListBox.DataSource = types;
        }

        private void betweenDatesBtn_Click(object sender, EventArgs e)
        {
            DataTable placeholder = new DataTable();

            placeholder.Columns.Add("Result");
            placeholder.Rows.Add("Empty Query");

            DataSet ds = this.controller.FindVisitDataBetweenDates(this.startDateTimePicker.Value,
                this.endDateTimePicker.Value);
            this.dateVisitDGV.DataSource = (ds.Tables.Count > 0) ? ds.Tables[0] : placeholder;
        }
    }
}
