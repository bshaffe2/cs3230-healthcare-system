﻿using System;

namespace HealthcareSystem.Model
{
    public class Schedule
    {
        /// <summary>
        /// Gets or sets the doctor's personID for the scheduled appointment.
        /// </summary>
        /// <value>
        /// The doctor's personID.
        /// </value>
        public int DoctorId { get; set; }

        /// <summary>
        /// Gets or sets the doctor's name.
        /// </summary>
        /// <value>
        /// The doctor's name.
        /// </value>
        public string Dname { get; set; }

        /// <summary>
        /// Gets or sets the date of the scheduled appointment.
        /// </summary>
        /// <value>
        /// The datetime.
        /// </value>
        public DateTime Datetime { get; set; }

        /// <summary>
        /// Gets or sets the patient's personID for the scheduled appointment.
        /// </summary>
        /// <value>
        /// The patient's personID.
        /// </value>
        public int PatientId { get; set; }

        /// <summary>
        /// Gets or sets the patient's name.
        /// </summary>
        /// <value>
        /// The patient's name.
        /// </value>
        public string Pname { get; set; }

        /// <summary>
        /// Gets or sets the reason for the appointment.
        /// </summary>
        /// <value>
        /// The reason.
        /// </value>
        public string Reason { get; set; }

        /// <summary>
        /// Gets or sets the schedule identifier.
        /// </summary>
        /// <value>
        /// The schedule identifier.
        /// </value>
        public int ScheduleId { get; set; }

        /// <summary>
        /// Initializes a new Schedule object (schedules an appointment) without specifying the scheduleID.
        /// </summary>
        /// <param name="doctorId">The doctor's personID.</param>
        /// <param name="datetime">The date for the appointment.</param>
        /// <param name="patientId">The patient's personID.</param>
        /// <param name="reason">The reason for the appointment.</param>
        public Schedule(int doctorId, DateTime datetime, int patientId, string reason)
        {
            this.DoctorId = doctorId;
            this.Datetime = datetime;
            this.PatientId = patientId;
            this.Reason = reason;
        }

        /// <summary>
        /// Initializes a new Schedule object (schedules an appointment).
        /// </summary>
        /// <param name="scheduleId">The schedule's scheduleID.</param>
        /// <param name="doctorId">The doctor's personID.</param>
        /// <param name="datetime">The date for the appointment.</param>
        /// <param name="patientId">The patient's personID.</param>
        /// <param name="reason">The reason for the appointment.</param>
        public Schedule(int scheduleId, int doctorId, DateTime datetime, int patientId, string reason)
        {
            this.DoctorId = doctorId;
            this.Datetime = datetime;
            this.PatientId = patientId;
            this.Reason = reason;
            this.ScheduleId = scheduleId;
        }
    }

}
