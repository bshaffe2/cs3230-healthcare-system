﻿using System;

namespace HealthcareSystem.Model
{
    public class Appointment
    {
        /// <summary>
        /// Gets or sets the (optional) diagnosis.
        /// </summary>
        /// <value>
        /// The diagnosis.
        /// </value>
        public string Diagnosis { get; set; }

        /// <summary>
        /// Gets or sets the reason for appointment
        /// </summary>
        /// <value>
        /// The appointment reason.
        /// </value>
        public string Reason { get; set; }

        /// <summary>
        /// Gets or sets the appointment date.
        /// </summary>
        /// <value>
        /// The appointment date.
        /// </value>
        public DateTime AppointmentDate { get; set; }

        /// <summary>
        /// Gets or sets the diastolic pressure value.
        /// </summary>
        /// <value>
        /// The diastolic pressure value.
        /// </value>
        public string Diastolic { get; set; }

        /// <summary>
        /// Gets or sets a value indicating whether there is a Hepatitis A Test.
        /// </summary>
        /// <value>
        ///   <c>true</c> if there is a Hepatitis A Test; otherwise, <c>false</c>.
        /// </value>
        public bool HepATest { get; set; }

        /// <summary>
        /// Gets or sets a value indicating whether there is a Hepatitis B Test.
        /// </summary>
        /// <value>
        ///   <c>true</c> if there is a Hepatitis B Test; otherwise, <c>false</c>.
        /// </value>
        public bool HepBTest { get; set; }

        /// <summary>
        /// Gets or sets a value indicating whether there is a Low Density Lipoproteins Test.
        /// </summary>
        /// <value>
        ///   <c>true</c> if there is a Low Density Lipoproteins Test; otherwise, <c>false</c>.
        /// </value>
        public bool LdlTest { get; set; }

        /// <summary>
        /// Gets or sets the pulse value.
        /// </summary>
        /// <value>
        /// The recorded pulse value.
        /// </value>
        public string Pulse { get; set; }

        /// <summary>
        /// Gets or sets the appointment's scheduleID.
        /// </summary>
        /// <value>
        /// The appointment's scheduleID.
        /// </value>
        public int ScheduleId { get; set; }

        /// <summary>
        /// Gets or sets the systolic pressure value.
        /// </summary>
        /// <value>
        /// The systolic pressure value.
        /// </value>
        public string Systolic { get; set; }

        /// <summary>
        /// Gets or sets the recorded temperature value.
        /// </summary>
        /// <value>
        /// The recorded temperature value.
        /// </value>
        public string Temperature { get; set; }

        /// <summary>
        /// Gets or sets a value indicating whether there is a White Blood Cell Test.
        /// </summary>
        /// <value>
        ///   <c>true</c> if there is a White Blood Cell Test; otherwise, <c>false</c>.
        /// </value>
        public bool WhiteBloodCellTest { get; set; }

        /// <summary>
        /// Gets or sets the appointment's nurseID.
        /// </summary>
        /// <value>
        /// The appointment's nurseID.
        /// </value>
        public int NurseId { get; set; }

        /// <summary>
        /// Gets or sets the diagnosis ID.
        /// </summary>
        /// <value>
        /// The diagnosis ID.
        /// </value>
        public int DiagnosisId { get; set; }

        /// <summary>
        /// Gets or sets the test ordered for appointment.
        /// </summary>
        /// <value>
        /// The test name.
        /// </value>
        public string TestName { get; set; }

        /// <summary>
        /// Gets or sets the date test was performed.
        /// </summary>
        /// <value>
        /// The perform date of test.
        /// </value>
        public DateTime TestDate { get; set; }

        /// <summary>
        /// Gets or sets the result of test.
        /// </summary>
        /// <value>
        /// The test result.
        /// </value>
        public string TestResult { get; set; }

        /// <summary>
        /// Creates a new Appointment object, based on the existing scheduled appointment (Schedule object).
        /// </summary>
        /// <param name="scheduleId">The schedule identifier.</param>
        /// <param name="diastolic">The diastolic pressure value.</param>
        /// <param name="systolic">The systolic pressure value.</param>
        /// <param name="temperature">The temperature value.</param>
        /// <param name="pulse">The pulse value.</param>
        /// <param name="diagnosis">The diagnosis.</param>
        /// <param name="whiteBloodCellTest">If there is a White Blood Cell Test.</param>
        /// <param name="ldlTest">If there is a Low Density Lipoprotein Test.</param>
        /// <param name="hepATest">If there is a Hepatitis A Test.</param>
        /// <param name="hepBTest">If there is a Hepatitis B Test.</param>
        /// <param name="nurseId">The personID of the nurse performing and recording values from the checkup.</param>
        public Appointment(int scheduleId, string diastolic, string systolic, string temperature, string pulse, string diagnosis, bool whiteBloodCellTest, bool ldlTest, bool hepATest, bool hepBTest, int nurseId)
        {
            this.ScheduleId = scheduleId;
            this.Diastolic = diastolic;
            this.Systolic = systolic;
            this.Temperature = temperature;
            this.Pulse = pulse;
            this.Diagnosis = diagnosis;
            this.WhiteBloodCellTest = whiteBloodCellTest;
            this.LdlTest = ldlTest;
            this.HepATest = hepATest;
            this.HepBTest = hepBTest;
            this.NurseId = nurseId;
        }
    }
}
