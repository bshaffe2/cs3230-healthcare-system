﻿using System;

namespace HealthcareSystem.Model
{
    public class Patient : Person
    {
        /// <summary>
        /// Creates a new patient.
        /// </summary>
        /// <param name="fname">The first name.</param>
        /// <param name="lname">The last name.</param>
        /// <param name="dob">The date of birth.</param>
        /// <param name="street">The street.</param>
        /// <param name="address2">The (optional) second street address.</param>
        /// <param name="city">The city.</param>
        /// <param name="state">The state.</param>
        /// <param name="zip">The zip code.</param>
        /// <param name="phone">The phone number.</param>
        /// <param name="gender">The gender.</param>
        public Patient(string fname, string lname, DateTime dob, string street, string address2, string city, string state, string zip, string phone, char gender)
        {
            this.Fname = fname;
            this.Lname = lname;
            this.Dob = dob;
            this.Street = street;
            this.Address2 = address2;
            this.City = city;
            this.State = state;
            this.Zipcode = zip;
            this.Phone = phone;
            this.Gender = gender;
            this.Type = PersonType.Patient;
        }
    }
}
