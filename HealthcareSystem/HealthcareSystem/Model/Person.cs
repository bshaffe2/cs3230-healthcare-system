﻿using System;

namespace HealthcareSystem.Model
{
    public abstract class Person
    {
        /// <summary>
        /// Gets or sets the person's personID.
        /// </summary>
        /// <value>
        /// The person's personID.
        /// </value>
        public int PersonId { get; set; }

        /// <summary>
        /// Gets or sets the first name of the person.
        /// </summary>
        /// <value>
        /// The first name of the person.
        /// </value>
        public string Fname { get; set; }

        /// <summary>
        /// Gets or sets the person's last name.
        /// </summary>
        /// <value>
        /// The person's last name.
        /// </value>
        public string Lname { get; set; }

        /// <summary>
        /// Gets or sets the person's date of birth.
        /// </summary>
        /// <value>
        /// The person's date of birth.
        /// </value>
        public DateTime Dob { get; set; }

        /// <summary>
        /// Gets or sets the person's street address.
        /// </summary>
        /// <value>
        /// The person's street address.
        /// </value>
        public string Street { get; set; }

        /// <summary>
        /// Gets or sets the person's (optional) second street address.
        /// </summary>
        /// <value>
        /// The person's second street address.
        /// </value>
        public string Address2 { get; set; }

        /// <summary>
        /// Gets or sets the person's city.
        /// </summary>
        /// <value>
        /// The person's city.
        /// </value>
        public string City { get; set; }

        /// <summary>
        /// Gets or sets the person's state.
        /// </summary>
        /// <value>
        /// The person's state.
        /// </value>
        public string State { get; set; }

        /// <summary>
        /// Gets or sets the person's zip code.
        /// </summary>
        /// <value>
        /// The person's zip code.
        /// </value>
        public string Zipcode { get; set; }

        /// <summary>
        /// Gets or sets the person's phone number.
        /// </summary>
        /// <value>
        /// The person's phone number.
        /// </value>
        public string Phone { get; set; }

        /// <summary>
        /// Gets or sets the type of person.
        /// </summary>
        /// <value>
        /// The type of person.
        /// </value>
        public PersonType Type { get; set; }

        /// <summary>
        /// Gets or sets the person's gender (i.e., nurse, patient, doctor, or administrator). 
        /// </summary>
        /// <value>
        /// The person's gender.
        /// </value>
        public char Gender { get; set; }
    }

    /// <summary>
    /// The different types of person a Person object could be.
    /// </summary>
    public enum PersonType
    {
        Patient,
        Nurse,
        Doctor,
        Administrator
    }
}
