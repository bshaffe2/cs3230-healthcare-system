﻿using System;

namespace HealthcareSystem.Model
{
    public class Test
    {
        public int DiagnosisId { get; set; }
        public TestType Type { get; set; }
        public DateTime PerformedOn { get; set; }
        public string Result { get; set; }
        public string FinalDiagnosis { get; set; }

        /// <summary>
        /// Creates a new Test.
        /// </summary>
        /// <param name="diagnosisId">The diagnosisID.</param>
        /// <param name="type">The test type.</param>
        /// <param name="result">The test result.</param>
        /// <param name="performedOn">The date test was performed on.</param>
        public Test(int diagnosisId, TestType type, DateTime performedOn, string result) {
            this.DiagnosisId = diagnosisId;
            this.Type = type;
            this.PerformedOn = performedOn;
            this.Result = result;
        }
        
    }

    /// <summary>
    /// Enumeration of test types.
    /// </summary>
    public enum TestType
    {
        WhiteBloodCell = 1,
        LowDensityLipoProteins = 2,
        HepatitisA = 3,
        HepatitisB = 4
    }
}
