﻿using System;

namespace HealthcareSystem.Model
{
    public class User : Person
    {
        /// <summary>
        /// Gets or sets the username of the User.
        /// </summary>
        /// <value>
        /// The username.
        /// </value>
        public string Username { get; set; }

        /// <summary>
        /// Gets or sets the password.
        /// </summary>
        /// <value>
        /// The password.
        /// </value>
        public string Password { get; set; }

        /// <summary>
        /// Initializes a new User.
        /// </summary>
        /// <param name="fname">The first name.</param>
        /// <param name="lname">The last name.</param>
        /// <param name="dob">The date of birth.</param>
        /// <param name="street">The street.</param>
        /// <param name="address2">The optional second address (may be null).</param>
        /// <param name="city">The city.</param>
        /// <param name="state">The state.</param>
        /// <param name="zip">The zip code.</param>
        /// <param name="phone">The phone number.</param>
        /// <param name="username">The username.</param>
        /// <param name="password">The password.</param>
        /// <param name="gender">The gender.</param>
        /// <param name="type">The type of user.</param>
        public User(string fname, string lname, DateTime dob, string street, string address2, string city, string state, string zip, string phone, string username, string password, char gender, PersonType type)
        {
            this.Fname = fname;
            this.Lname = lname;
            this.Dob = dob;
            this.Street = street;
            this.Address2 = address2;
            this.City = city;
            this.State = state;
            this.Zipcode = zip;
            this.Phone = phone;
            this.Gender = gender;
            this.Username = username;
            this.Password = password;
            this.Type = type;
        }
    }
}
