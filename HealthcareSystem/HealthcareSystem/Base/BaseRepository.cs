﻿using System.Collections.Generic;

namespace HealthcareSystem.Base
{
    public abstract class BaseRepository<T>
    {
        protected string ConnectionLabel;

        /// <summary>
        /// Initializes the BaseRepository.
        /// </summary>
        protected BaseRepository()
        {
            this.ConnectionLabel = "MySqlDbConnection";
        }
        /// <summary>
        /// Adds the specified entry.
        /// </summary>
        /// <param name="entry">The entry.</param>
        public abstract void Add(T entry);

        /// <summary>
        /// Gets the T object by identifier.
        /// </summary>
        /// <param name="id">The identifier.</param>
        /// <returns>The corresponding T object</returns>
        public abstract T GetById(int id);

        /// <summary>
        /// Gets all T objects.
        /// </summary>
        /// <returns>A list of T objects</returns>
        public abstract IList<T> GetAll();
    }
}
