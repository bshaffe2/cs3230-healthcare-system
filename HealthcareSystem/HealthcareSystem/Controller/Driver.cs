﻿using System;
using System.Windows.Forms;
using HealthcareSystem.View;

namespace HealthcareSystem.Controller
{
    static class Driver
    {
        /// <summary>
        /// The main entry point for the application.
        /// </summary>
        [STAThread]
        static void Main()
        {
            RepositoryController controller = new RepositoryController();
            Application.EnableVisualStyles();
            Application.SetCompatibleTextRenderingDefault(false);
            Application.Run(new HrSystemView(controller));
        }
    }
}
