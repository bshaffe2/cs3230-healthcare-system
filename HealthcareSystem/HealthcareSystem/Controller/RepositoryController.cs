﻿using System;
using System.Collections.Generic;
using System.Data;
using HealthcareSystem.DAL.Repository;
using HealthcareSystem.Model;

namespace HealthcareSystem.Controller
{
    public class RepositoryController
    {
        private PatientRepository patientRepo;
        private UserRepository userRepo;
        private AdminRepository adminRepo;
        private NurseRepository nurseRepo;
        private DoctorRepository doctorRepo;
        private ScheduleRepository scheduleRepo;
        private AppointmentRepository appointmentRepo;
        private TestRepository testRepo;
        public User CurrentUser { get; set; }

        /// <summary>
        /// Initializes a new RepositoryController.
        /// </summary>
        public RepositoryController()
        {
            this.patientRepo = new PatientRepository();
            this.userRepo = new UserRepository();
            this.adminRepo = new AdminRepository();
            this.nurseRepo = new NurseRepository();
            this.doctorRepo = new DoctorRepository();
            this.scheduleRepo = new ScheduleRepository();
            this.appointmentRepo = new AppointmentRepository();
            this.testRepo = new TestRepository();
        }

        /// <summary>
        /// Validates the login form.
        /// </summary>
        /// <param name="username">The login username.</param>
        /// <param name="password">The login password.</param>
        /// <returns></returns>
        public bool ValidateLogin(string username, string password)
        {
            this.CurrentUser = this.userRepo.IsValidLogin(username, password);
            return this.CurrentUser != null;
        }

        /// <summary>
        /// Adds the patient.
        /// </summary>
        /// <param name="fname">The patient's first name.</param>
        /// <param name="lname">The patient's last name.</param>
        /// <param name="dob">The patient's date of birth.</param>
        /// <param name="street">The patient's street address.</param>
        /// <param name="address2">The patient's (optional) second address.</param>
        /// <param name="city">The patient's city.</param>
        /// <param name="state">The patient's state.</param>
        /// <param name="zip">The patient's zip code.</param>
        /// <param name="phone">The patient's phone number.</param>
        /// <param name="gender">The patient's gender.</param>
        public void AddPatient(string fname, string lname, DateTime dob, string street, string address2, string city, string state, string zip, string phone, char gender)
        { 
            Patient patient = new Patient(fname, lname, dob, street, address2, city, state, zip, phone, gender);

            this.patientRepo.Add(patient);
        }

        /// <summary>
        /// Registers a new user.
        /// </summary>
        /// <param name="fname">The new user's first name.</param>
        /// <param name="lname">The new user's  last name.</param>
        /// <param name="dob">The new user's date of bird.</param>
        /// <param name="street">The new user's street.</param>
        /// <param name="address2">The new user's (optional) second address.</param>
        /// <param name="city">The new user's city.</param>
        /// <param name="state">The new user's state.</param>
        /// <param name="zip">The new user's zip code.</param>
        /// <param name="phone">The new user's phone.</param>
        /// <param name="gender">The new user's gender.</param>
        /// <param name="username">The new user's username.</param>
        /// <param name="password">The new user's password.</param>
        /// <param name="isAdmin">If the user is an admin type user.</param>
        /// <exception cref="InvalidOperationException">Username already exists.</exception>
        public void RegisterUser(string fname, string lname, DateTime dob, string street, string address2, string city, string state, string zip, string phone, char gender, string username, string password, bool isAdmin)
        {
            PersonType userType;

            if (isAdmin)
            {
                userType = PersonType.Administrator;
            }
            else
            {
                userType = PersonType.Nurse;
            }

            User user = new User(fname, lname, dob, street, address2, city, state, zip, phone, username, password, gender, userType);

            if (!this.userRepo.UserExists(user))
            {
                    if (user.Type == PersonType.Administrator) {
                        Administrator admin = new Administrator(fname, lname, dob, street, address2, city, state, zip, phone, gender);
                        this.adminRepo.AddAsUser(admin, user);
                    } else if (user.Type == PersonType.Nurse) {
                        Nurse nurse = new Nurse(fname, lname, dob, street, address2, city, state, zip, phone, gender);
                        this.nurseRepo.AddAsUser(nurse, user);
                    }
            }
            else
            {
                throw new InvalidOperationException("Username already exists.");
            }          
        }

        /// <summary>
        /// Returns the doctors.
        /// </summary>
        /// <returns>A list of all doctors.</returns>
        public IList<Doctor> GetDoctors()
        {
            return this.doctorRepo.GetAll();
        }

        /// <summary>
        /// Returns the patients.
        /// </summary>
        /// <returns>A list of all patients</returns>
        public IList<Patient> GetPatients()
        {
            return this.patientRepo.GetAll();
        }

        /// <summary>
        /// Returns the scheduled appointments.
        /// </summary>
        /// <returns>A list of all scheduled appointments</returns>
        public IList<Schedule> GetSchedules()
        {
            return this.scheduleRepo.GetAll();
        }

        /// <summary>
        /// Adds the schedule to the list of scheduled appointments.
        /// </summary>
        /// <param name="apptDoctor">The appointment's doctor.</param>
        /// <param name="apptDate">The appointment's date.</param>
        /// <param name="apptPatient">The appointment's patient.</param>
        /// <param name="reason">The reason for the appointment.</param>
        public void AddSchedule(Doctor apptDoctor, DateTime apptDate, Patient apptPatient, string reason)
        {
            Schedule schedule = new Schedule(apptDoctor.PersonId, apptDate, apptPatient.PersonId, reason);
            this.scheduleRepo.Add(schedule);
        }

        /// <summary>
        /// Runs the admin's query.
        /// </summary>
        /// <param name="sql">The SQL.</param>
        /// <returns>A DataSet containing the query's result.</returns>
        public DataSet RunAdminQuery(string sql)
        {
            return this.adminRepo.GetQueryResults(sql);
        }

        /// <summary>
        /// Searches the patients.
        /// </summary>
        /// <param name="fname">The provided first name.</param>
        /// <param name="lname">The provided last name.</param>
        /// <param name="bdate">The birth date.</param>
        /// <returns>A DataSet representing the patient information.</returns>
        public DataSet SearchPatients(string fname, string lname, DateTime bdate)
        {
            DataSet patients = this.patientRepo.SearchPatient(fname, lname, bdate);
            return patients;
        }

        /// <summary>
        /// Adds the checkup to the specified appointment.
        /// </summary>
        /// <param name="scheduled">The scheduled appointment.</param>
        /// <param name="diastolic">The diastolic pressure value.</param>
        /// <param name="systolic">The systolic pressure value.</param>
        /// <param name="temperature">The patient's temperature.</param>
        /// <param name="pulse">The patient's pulse.</param>
        /// <param name="diagnosis">The (optional) diagnosis.</param>
        /// <param name="whiteBloodCellTest">If there's a White Blood Cell Test.</param>
        /// <param name="ldlTest">If there's a Low DEnsity Lipoproteins Test.</param>
        /// <param name="hepATest">If there's a Hepatitis A test.</param>
        /// <param name="hepBTest">If there's a Hepatitus B test.</param>
        /// <param name="nurseId">The nurse's personID, who is doing the checkup.</param>
        public void AddCheck(Schedule scheduled, string diastolic, string systolic, 
            string temperature, string pulse, string diagnosis, bool whiteBloodCellTest, 
            bool ldlTest, bool hepATest, bool hepBTest, int nurseId)
        {
            Appointment appt = new Appointment(scheduled.ScheduleId, diastolic, 
                systolic, temperature, pulse, diagnosis, whiteBloodCellTest, 
                ldlTest, hepATest, hepBTest, nurseId);
            this.appointmentRepo.Add(appt);
        }

        /// <summary>
        /// Searches the appointments for those matching a patient ID
        /// </summary>
        /// <param name="patientId">The patient's ID.</param>
        /// <returns>A list representing the appointment visit information.</returns>
        public IList<Appointment> SearchVisitInfo(int patientId)
        {
            return this.appointmentRepo.SearchById(patientId);
        }
        /// <summary>
        /// Updates test and diagnosis info for a diagnosis ID
        /// </summary>
        /// <param name="diagnosisId">The diagnosisID.</param>
        /// <param name="type">The test type.</param>
        /// <param name="result">The test result.</param>
        /// <param name="diagnosis">The diagnosis.</param>
        /// <param name="performed">The date test was performed on.</param>
        public void UpdateTestAndDiagnosis(int diagnosisId, TestType type, string result, string diagnosis, DateTime performed)
        {
            this.testRepo.UpdateTestResults(diagnosisId, type, result, diagnosis, performed);
        }

        /// <summary>
        /// Finds the visit data between the provided dates.
        /// </summary>
        /// <param name="startDate">The start date.</param>
        /// <param name="endDate">The end date.</param>
        /// <returns>DataSet representing the visit data between the provided dates</returns>
        public DataSet FindVisitDataBetweenDates(DateTime startDate, DateTime endDate)
        {
            return this.adminRepo.GetVisitResultBetweenDates(startDate, endDate);
        }
    }
}
