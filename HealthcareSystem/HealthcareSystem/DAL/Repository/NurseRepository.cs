﻿using HealthcareSystem.Base;
using HealthcareSystem.Model;
using MySql.Data.MySqlClient;
using System;
using System.Collections.Generic;

namespace HealthcareSystem.DAL.Repository
{
    public class NurseRepository : BaseRepository<Nurse>
    {
        /// <summary>
        /// Adds the specified nurse as a new user.
        /// </summary>
        /// <param name="nurse">The nurse to add.</param>
        /// <param name="user">The new user.</param>
        public void AddAsUser(Nurse nurse, User user)
        {
            string insertToPersonSQL = @"INSERT INTO person (fname, lname, dob, street, address2, city, state, zipcode, phone, gender) VALUES (@fname, @lname, @dob, @street, @address2, @city, @state, @zip, @phone, @gender)";
            string insertToNurseSQL = @"INSERT INTO nurse (personID) VALUES (last_insert_id())";
            string insertToUserSQL = @"INSERT INTO users (personID, username, password) VALUES (last_insert_id(), @username, @password)";

            try
            {
                using (MySqlConnection connection = DBConnection.GetConnection(this.ConnectionLabel))
                {
                    connection.Open();

                    using (MySqlCommand cmd = new MySqlCommand(insertToPersonSQL, connection))
                    {
                        cmd.Parameters.AddWithValue("@fname", nurse.Fname);
                        cmd.Parameters.AddWithValue("@lname", nurse.Lname);
                        cmd.Parameters.AddWithValue("@dob", nurse.Dob);
                        cmd.Parameters.AddWithValue("@street", nurse.Street);
                        cmd.Parameters.AddWithValue("@address2", nurse.Address2);
                        cmd.Parameters.AddWithValue("@city", nurse.City);
                        cmd.Parameters.AddWithValue("@state", nurse.State);
                        cmd.Parameters.AddWithValue("@zip", nurse.Zipcode);
                        cmd.Parameters.AddWithValue("@phone", nurse.Phone);
                        cmd.Parameters.AddWithValue("@gender", nurse.Gender);
                        cmd.ExecuteNonQuery();
                    }

                    using (MySqlCommand cmd = new MySqlCommand(insertToNurseSQL, connection))
                    {
                        cmd.ExecuteNonQuery();
                    }

                    using (MySqlCommand cmd = new MySqlCommand(insertToUserSQL, connection))
                    {
                        cmd.Parameters.AddWithValue("@username", user.Username);
                        cmd.Parameters.AddWithValue("@password", user.Password);
                        cmd.ExecuteNonQuery();

                    }
                }
            }
            catch (MySqlException e)
            {
                throw;
            }
        }

        /// <summary>
        /// Gets the nurse by their personID.
        /// </summary>
        /// <param name="id">The personID.</param>
        /// <returns>The corresponding Nurse</returns>
        /// <exception cref="NotImplementedException"></exception>
        public override Nurse GetById(int id)
        {
            throw new NotImplementedException();
        }

        /// <summary>
        /// Gets all nurses.
        /// </summary>
        /// <returns>A list of all nurses</returns>
        /// <exception cref="NotImplementedException"></exception>
        public override IList<Nurse> GetAll()
        {
            throw new NotImplementedException();
        }

        /// <summary>
        /// Adds the specified nurse.
        /// </summary>
        /// <param name="entry">The specified Nurse.</param>
        /// <exception cref="NotImplementedException"></exception>
        public override void Add(Nurse entry)
        {
            throw new NotImplementedException();
        }
    }
}
