﻿using HealthcareSystem.Base;
using HealthcareSystem.Model;
using MySql.Data.MySqlClient;
using System;
using System.Collections.Generic;

namespace HealthcareSystem.DAL.Repository
{
    public class TestRepository : BaseRepository<Test>
    {
        /// <summary>
        /// Adds the specified test.
        /// </summary>
        /// <param name="entry">The entry.</param>
        public override void Add(Test entry)
        {
            throw new NotImplementedException();
        }

        /// <summary>
        /// Gets the specified test.
        /// </summary>
        /// <param name="id">The test to get.</param>
        public override Test GetById(int id)
        {
            throw new NotImplementedException();
        }

        /// <summary>
        /// Gets all of the tests.
        /// </summary>
        /// <returns>A list of all tests</returns>
        public override IList<Test> GetAll()
        {
            throw new NotImplementedException();
        }

        /// <summary>
        /// Updates visit info for a diagnosisID
        /// </summary>
        /// <param name="diagnosisId">The diagnosisID.</param>
        /// <param name="type">The test type.</param>
        /// <param name="result">The test result.</param>
        /// <param name="diagnosis">The diagnosis.</param>
        /// <param name="performed">The date test was performed on.</param>
        public void UpdateTestResults(int diagnosisId, TestType type, string result, string diagnosis, DateTime performed)
        {
            string sql = @"
                UPDATE 
	                diagnosis JOIN test_diagnosis USING (diagnosisID)
                SET";
            if (!String.IsNullOrEmpty(diagnosis))
            {
                sql += @" diagnosis.diagnosis = @diagnosis,";
            }

            if (performed.Date <= DateTime.Now.Date)
            {
                sql += @" test_diagnosis.dateperformed = @performed,";
            }

            if (!String.IsNullOrEmpty(result))
            {
                sql += @" test_diagnosis.result = @result,";
            }

            sql += @"testID = @testType
                WHERE
                    test_diagnosis.testID = @testType
                    AND test_diagnosis.diagnosisID = @diagnosisID";

            try
            {
                using (MySqlConnection connection = DBConnection.GetConnection(this.ConnectionLabel))
                {
                    connection.Open();

                    using (MySqlCommand cmd = new MySqlCommand(sql, connection))
                    {
                        cmd.Parameters.AddWithValue("@diagnosisID", diagnosisId);
                        cmd.Parameters.AddWithValue("@testType", type);
                        cmd.Parameters.AddWithValue("@result", result);
                        cmd.Parameters.AddWithValue("@diagnosis", diagnosis);
                        cmd.Parameters.AddWithValue("@performed", performed.Date);
                        
                        int count = cmd.ExecuteNonQuery();

                        if (count < 1) {
                            throw new InvalidOperationException("Insufficient data for update");
                        }
                    }
                }
            }
            catch
            {
                throw;
            }
        }
    }
}
