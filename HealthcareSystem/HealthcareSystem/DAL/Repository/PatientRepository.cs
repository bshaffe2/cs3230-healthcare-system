﻿using System;
using System.Collections.Generic;
using System.Data;
using HealthcareSystem.Base;
using HealthcareSystem.Model;
using MySql.Data.MySqlClient;

namespace HealthcareSystem.DAL.Repository
{
    public class PatientRepository : BaseRepository<Patient>
    {
        /// <summary>
        /// Adds the specified patient.
        /// </summary>
        /// <param name="entry">The specified patient.</param>
        public override void Add(Patient entry)
        {
            string insertToPersonSQL = @"INSERT INTO person (fname, lname, dob, street, address2, city, state, zipcode, phone, gender) VALUES (@fname, @lname, @dob,@street, @address2, @city, @state, @zip, @phone, @gender)";
            string insertToPatientSQL = @"INSERT INTO patient (personID) VALUES (last_insert_id())";
            
            try
            {
                using (MySqlConnection connection = DBConnection.GetConnection(this.ConnectionLabel))
                {
                    connection.Open();

                    using (MySqlCommand cmd = new MySqlCommand(insertToPersonSQL, connection))
                    {
                        cmd.Parameters.AddWithValue("@fname", entry.Fname);
                        cmd.Parameters.AddWithValue("@lname", entry.Lname);
                        cmd.Parameters.AddWithValue("@dob", entry.Dob);
                        cmd.Parameters.AddWithValue("@street", entry.Street);
                        cmd.Parameters.AddWithValue("@address2", entry.Address2);
                        cmd.Parameters.AddWithValue("@city", entry.City);
                        cmd.Parameters.AddWithValue("@state", entry.State);
                        cmd.Parameters.AddWithValue("@zip", entry.Zipcode);
                        cmd.Parameters.AddWithValue("@phone", entry.Phone);
                        cmd.Parameters.AddWithValue("@gender", entry.Gender);
                        cmd.ExecuteNonQuery();
                    }

                    using (MySqlCommand cmd = new MySqlCommand(insertToPatientSQL, connection))
                    {
                        cmd.ExecuteNonQuery();
                    }
                }
            }
            catch (MySqlException e)
            {
                throw;
            }            
        }

        /// <summary>
        /// Gets all patients.
        /// </summary>
        /// <returns>A list of all patients</returns>
        public override IList<Patient> GetAll()
        {
            IList<Patient> patients = new List<Patient>();
            try
            {
                using (MySqlConnection connection = DBConnection.GetConnection(this.ConnectionLabel))
                {
                    connection.Open();

                    using (MySqlCommand cmd = new MySqlCommand("sp_getAllPatients", connection))
                    {
                        cmd.CommandType = CommandType.StoredProcedure;
                        using (MySqlDataReader reader = cmd.ExecuteReader())
                        {
                            while (reader.Read())
                            {
                                int personId = (!reader.IsDBNull(0) ? reader.GetInt32(0) : 0);
                                string fname = (!reader.IsDBNull(1)) ? reader.GetString(1) : "";
                                string lname = (!reader.IsDBNull(2)) ? reader.GetString(2) : "";
                                DateTime dob = (!reader.IsDBNull(3)) ? reader.GetDateTime(3) : DateTime.MinValue;
                                string street = (!reader.IsDBNull(4)) ? reader.GetString(4) : "";
                                string address2 = (!reader.IsDBNull(5)) ? reader.GetString(5) : "";
                                string city = (!reader.IsDBNull(6)) ? reader.GetString(6) : "";
                                string state = (!reader.IsDBNull(7)) ? reader.GetString(7) : "";
                                string zipcode = (!reader.IsDBNull(8)) ? reader.GetString(8) : "";
                                string phone = (!reader.IsDBNull(9)) ? reader.GetString(9) : "";
                                char gender = (!reader.IsDBNull(10)) ? reader.GetChar(10) : ' ';

                                Patient patient = new Patient(fname, lname, dob, street, address2, city, state, zipcode, phone, gender);
                                patient.PersonId = personId;
                                patients.Add(patient);
                            }
                        }
                    }
                }

                return patients;
            }
            catch (MySqlException e)
            {
                throw;
            }
        }

        /// <summary>
        /// Gets the patient by their personID.
        /// </summary>
        /// <param name="id">The personID.</param>
        /// <returns>The corresponding Patient</returns>
        /// <exception cref="NotImplementedException"></exception>
        public override Patient GetById(int id)
        {
            throw new NotImplementedException();
        }

        /// <summary>
        /// Searches for the patient based on certain criteria.
        /// </summary>
        /// <param name="fname">The potential Patient's first name.</param>
        /// <param name="lname">The potential Patient's last name.</param>
        /// <param name="bdate">The potential Patient's birth date.</param>
        /// <returns>A DataSet with the Patient info.</returns>
        public DataSet SearchPatient(string fname, string lname, DateTime bdate)
        {
            string sqlQuery = "SELECT person.* FROM patient, person WHERE patient.personID = person.personID AND fname LIKE @fname AND lname LIKE @lname AND dob LIKE @dob";
            DataSet patients = new DataSet();
            try
            {
                using (MySqlConnection connection = DBConnection.GetConnection(this.ConnectionLabel))
                {
                    connection.Open();

                    using (MySqlCommand cmd = new MySqlCommand(sqlQuery, connection))
                    {
                        cmd.Parameters.AddWithValue("@fname", "%"+fname+"%");
                        cmd.Parameters.AddWithValue("@lname", "%"+lname+"%");
                        if (bdate.ToString("yyyy-MM-dd") == DateTime.Today.ToString("yyyy-MM-dd"))
                        {
                            cmd.Parameters.AddWithValue("@dob", "%");
                        }
                        else
                        {
                            cmd.Parameters.AddWithValue("@dob", "%"+bdate.ToString("yyyy-MM-dd")+"%");
                        }
                        using (MySqlDataAdapter adapter = new MySqlDataAdapter(cmd))
                        {
                            adapter.Fill(patients);
                        }
                    }
                }

                return patients;
            }
            catch (MySqlException e)
            {
                throw;
            }
        }
    }
}
