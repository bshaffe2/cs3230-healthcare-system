﻿using HealthcareSystem.Base;
using HealthcareSystem.Model;
using MySql.Data.MySqlClient;
using System;
using System.Collections.Generic;
using System.Data;
using System.Windows.Forms;

namespace HealthcareSystem.DAL.Repository
{
    /// <summary>
    /// 
    /// </summary>
    /// <seealso cref="HealthcareSystem.Base.BaseRepository{HealthcareSystem.Model.Administrator}" />
    public class AdminRepository : BaseRepository<Administrator>
    {
        /// <summary>
        /// Adds as user.
        /// </summary>
        /// <param name="admin">The admin.</param>
        /// <param name="user">The user.</param>
        public void AddAsUser(Administrator admin, User user)
        {
            string insertToPersonSQL = @"INSERT INTO person (fname, lname, dob, address, phone, gender) VALUES (@fname, @lname, @dob, @street, @address2, @city, @state, @zip, @phone, @gender)";
            string insertToAdminSQL = @"INSERT INTO administrator (personID) VALUES (last_insert_id())";
            string insertToUserSQL = @"INSERT INTO users (personID, username, password) VALUES (last_insert_id(), @username, @password)";

            try
            {
                using (MySqlConnection connection = DBConnection.GetConnection(this.ConnectionLabel))
                {
                    connection.Open();

                    using (MySqlCommand cmd = new MySqlCommand(insertToPersonSQL, connection))
                    {
                        cmd.Parameters.AddWithValue("@fname", admin.Fname);
                        cmd.Parameters.AddWithValue("@lname", admin.Lname);
                        cmd.Parameters.AddWithValue("@dob", admin.Dob);
                        cmd.Parameters.AddWithValue("@street", admin.Street);
                        cmd.Parameters.AddWithValue("@address2", admin.Address2);
                        cmd.Parameters.AddWithValue("@city", admin.City);
                        cmd.Parameters.AddWithValue("@state", admin.State);
                        cmd.Parameters.AddWithValue("@zip", admin.Zipcode);
                        cmd.Parameters.AddWithValue("@phone", admin.Phone);
                        cmd.Parameters.AddWithValue("@gender", admin.Gender);
                        cmd.ExecuteNonQuery();
                    }

                    using (MySqlCommand cmd = new MySqlCommand(insertToAdminSQL, connection))
                    {
                        cmd.ExecuteNonQuery();
                    }

                    using (MySqlCommand cmd = new MySqlCommand(insertToUserSQL, connection))
                    {
                        cmd.Parameters.AddWithValue("@username", user.Username);
                        cmd.Parameters.AddWithValue("@password", user.Password);
                        cmd.ExecuteNonQuery();

                    }
                }
            }
            catch (MySqlException e)
            {
                throw;
            }
        }

        /// <summary>
        /// Gets the by identifier.
        /// </summary>
        /// <param name="id">The identifier.</param>
        /// <returns></returns>
        /// <exception cref="NotImplementedException"></exception>
        public override Administrator GetById(int id)
        {
            throw new NotImplementedException();
        }

        /// <summary>
        /// Gets all.
        /// </summary>
        /// <returns></returns>
        /// <exception cref="NotImplementedException"></exception>
        public override IList<Administrator> GetAll()
        {
            throw new NotImplementedException();
        }

        /// <summary>
        /// Adds the specified entry.
        /// </summary>
        /// <param name="entry">The entry.</param>
        /// <exception cref="NotImplementedException"></exception>
        public override void Add(Administrator entry)
        {
            throw new NotImplementedException();
        }

        /// <summary>
        /// Gets the query results.
        /// </summary>
        /// <param name="sql">The SQL.</param>
        /// <returns></returns>
        public DataSet GetQueryResults(string sql)
        {
            try
            {
                DataSet ds = new DataSet();
                using (MySqlConnection connection = DBConnection.GetConnection(this.ConnectionLabel))
                {
                    connection.Open();

                    using (MySqlDataAdapter adapter = new MySqlDataAdapter())
                    {
                        MySqlCommand cmd = connection.CreateCommand();
                        if (!String.IsNullOrWhiteSpace(sql))
                        {
                            cmd.CommandText = sql;
                            adapter.SelectCommand = cmd;
                            adapter.Fill(ds);
                        }
                    }
                }

                return ds;
            }
            catch (MySqlException e)
            {
                MessageBox.Show(e.Message);
                return new DataSet();
            }
        }

        /// <summary>
        /// Gets the visit results between two different dates.
        /// </summary>
        /// <param name="startDate">The start date.</param>
        /// <param name="endDate">The end date.</param>
        /// <returns>The DataSet representing the visit information</returns>
        public DataSet GetVisitResultBetweenDates(DateTime startDate, DateTime endDate)
        {
            try
            {
                DataSet ds = new DataSet();
                using (MySqlConnection connection = DBConnection.GetConnection(this.ConnectionLabel))
                {
                    connection.Open();

                    using (MySqlCommand cmd = new MySqlCommand("sp_visitDataBetweenDates", connection))
                    {
                        cmd.CommandType = CommandType.StoredProcedure;
                        cmd.Parameters.AddWithValue("@startDate", startDate);
                        cmd.Parameters.AddWithValue("@endDate", endDate);

                        using (MySqlDataAdapter adapter = new MySqlDataAdapter(cmd))
                        {
                            adapter.Fill(ds);
                        }
                    }
                }

                return ds;
            }
            catch(MySqlException e)
            {
                MessageBox.Show(e.ToString());
                return new DataSet();
            }

        }
    }
}
