﻿using System;
using System.Collections.Generic;
using HealthcareSystem.Base;
using HealthcareSystem.Model;
using MySql.Data.MySqlClient;

namespace HealthcareSystem.DAL.Repository
{
    public class DoctorRepository : BaseRepository<Doctor>
    {
        /// <summary>
        /// Adds the specified doctor.
        /// </summary>
        /// <param name="entry">The Doctor to add.</param>
        /// <exception cref="NotImplementedException"></exception>
        public override void Add(Doctor entry)
        {
            throw new NotImplementedException();
        }

        /// <summary>
        /// Gets all doctors.
        /// </summary>
        /// <returns>A list of all doctors.</returns>
        public override IList<Doctor> GetAll()
        {
            string sql = @" SELECT
                                *
                            FROM
                                doctor
                                JOIN person USING (personID)";
            IList<Doctor> doctors = new List<Doctor>();
            try
            {
                using (MySqlConnection connection = DBConnection.GetConnection(this.ConnectionLabel))
                {
                    connection.Open();

                    using (MySqlCommand cmd = new MySqlCommand(sql, connection))
                    {
                        using (MySqlDataReader reader = cmd.ExecuteReader())
                        {                            
                            while (reader.Read())
                            {
                                int personId = (!reader.IsDBNull(0) ? reader.GetInt32(0) : 0);
                                string fname = (!reader.IsDBNull(1)) ? reader.GetString(1) : "";
                                string lname = (!reader.IsDBNull(2)) ? reader.GetString(2) : "";
                                DateTime dob = (!reader.IsDBNull(3)) ? reader.GetDateTime(3) : DateTime.MinValue;
                                string street = (!reader.IsDBNull(4)) ? reader.GetString(4) : "";
                                string address2 = (!reader.IsDBNull(5)) ? reader.GetString(5) : "";
                                string city = (!reader.IsDBNull(6)) ? reader.GetString(6) : "";
                                string state = (!reader.IsDBNull(7)) ? reader.GetString(7) : "";
                                string zipcode = (!reader.IsDBNull(8)) ? reader.GetString(8) : "";
                                string phone = (!reader.IsDBNull(9)) ? reader.GetString(9) : "";
                                char gender = (!reader.IsDBNull(10)) ? reader.GetChar(10) : ' ';

                                Doctor doctor = new Doctor(fname, lname, dob, street, address2, city, state, zipcode, phone, gender);
                                doctor.PersonId = personId;
                                doctors.Add(doctor);
                            }
                        }                       
                    }
                }

                return doctors;
            }
            catch (MySqlException e)
            {                
                throw;
            }
        }

        /// <summary>
        /// Gets the Doctor by their personID.
        /// </summary>
        /// <param name="id">The personID.</param>
        /// <returns>The corresponding Doctor</returns>
        /// <exception cref="NotImplementedException"></exception>
        public override Doctor GetById(int id)
        {
            throw new NotImplementedException();
        }
    }
}
