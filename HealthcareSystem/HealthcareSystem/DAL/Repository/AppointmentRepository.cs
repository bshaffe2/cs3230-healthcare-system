﻿using HealthcareSystem.Base;
using HealthcareSystem.Model;
using System;
using System.Collections.Generic;
using MySql.Data.MySqlClient;

namespace HealthcareSystem.DAL.Repository
{
    public class AppointmentRepository : BaseRepository<Appointment>
    {
        /// <summary>
        /// Adds the specified appointment.
        /// </summary>
        /// <param name="entry">The entry.</param>
        public override void Add(Appointment entry)
        {
            string sql = @"
            START TRANSACTION;
	        INSERT INTO checks (diastolic, systolic, temperature, pulse, nurseID) 
    	        VALUES (@diastolic, @systolic, @temperature, @pulse, @nurseID);
            SET @checks = LAST_INSERT_ID();
            INSERT INTO diagnosis (diagnosis) VALUES (@diagnosis);
            SET @diagnose = LAST_INSERT_ID();
            INSERT INTO appointment (scheduleID, checkID, diagnosisID) 
    	        VALUES (@scheduleID, @checks, @diagnose);";

            if (entry.WhiteBloodCellTest)
            {
                sql += @"INSERT INTO test_diagnosis (testID, diagnosisID) VALUES (1, @diagnose);";
            }
            if (entry.LdlTest)
            {
                sql += @"INSERT INTO test_diagnosis (testID, diagnosisID) VALUES (2, @diagnose);";
            }
            if (entry.HepATest)
            {
                sql += @"INSERT INTO test_diagnosis (testID, diagnosisID) VALUES (3, @diagnose);";
            }
            if (entry.HepBTest)
            {
                sql += @"INSERT INTO test_diagnosis (testID, diagnosisID) VALUES (4, @diagnose);";
            }

            sql += @"COMMIT;";

            try
            {
                using (MySqlConnection connection = DBConnection.GetConnection(this.ConnectionLabel))
                {
                    connection.Open();

                    using (MySqlCommand cmd = new MySqlCommand(sql, connection))
                    {
                        cmd.Parameters.AddWithValue("@diastolic", entry.Diastolic);
                        cmd.Parameters.AddWithValue("@systolic", entry.Systolic);
                        cmd.Parameters.AddWithValue("@temperature", entry.Temperature);
                        cmd.Parameters.AddWithValue("@pulse", entry.Pulse);
                        cmd.Parameters.AddWithValue("@diagnosis", entry.Diagnosis);
                        cmd.Parameters.AddWithValue("@scheduleID", entry.ScheduleId);
                        cmd.Parameters.AddWithValue("@nurseID", entry.NurseId);

                        cmd.ExecuteNonQuery();
                    }
                }
            }
            catch (MySqlException e)
            {
                throw;
            }
        }


        /// <summary>
        /// Searches for appointment info by patient ID
        /// </summary>
        /// <param name="id">THe patient id.</param>
        public IList<Appointment> SearchById(int id)
        {
            string sql = @"
SELECT
	*
FROM
	appointment
    JOIN schedule s USING (scheduleID)
    JOIN checks c USING (checkID)
    JOIN diagnosis d USING (diagnosisID)
    LEFT JOIN 
    	(SELECT * FROM 
         test_diagnosis td 
         JOIN test USING (testID)
         ) td USING (diagnosisID)
WHERE s.patientID = @patientid";

            IList<Appointment> appts = new List<Appointment>();
            try
            {
                using (MySqlConnection connection = DBConnection.GetConnection(this.ConnectionLabel))
                {
                    connection.Open();

                    using (MySqlCommand cmd = new MySqlCommand(sql, connection))
                    {
                        cmd.Parameters.AddWithValue("@patientid", id);

                        using (MySqlDataReader reader = cmd.ExecuteReader())
                        {
                            while (reader.Read())
                            {
                                int scheduleId = (!reader.IsDBNull(2) ? reader.GetInt32(2) : 0);
                                DateTime apptdate = (!reader.IsDBNull(4) ? reader.GetDateTime(4) : DateTime.MinValue);
                                string reason = (!reader.IsDBNull(6) ? reader.GetString(6) : "No Reason");
                                int diastolic = (!reader.IsDBNull(7) ? reader.GetInt32(7) : 0);
                                int systolic = (!reader.IsDBNull(8) ? reader.GetInt32(8) : 0);
                                int temperature = (!reader.IsDBNull(9) ? reader.GetInt32(9) : 0);
                                int pulse = (!reader.IsDBNull(10) ? reader.GetInt32(10) : 0);
                                string diagnosis = (!(reader.IsDBNull(12) || String.IsNullOrEmpty(reader.GetString(12))) ? reader.GetString(12) : "No Diagnosis");
                                int diagnosisId = (!reader.IsDBNull(0) ? reader.GetInt32(0) : 0);
                                string testname = (!reader.IsDBNull(16) ? reader.GetString(16) : "No Tests Ordered");
                                DateTime testdate = (!reader.IsDBNull(14) ? reader.GetDateTime(14) : DateTime.MinValue);
                                string testresult = (!reader.IsDBNull(15) ? reader.GetString(15) : "No Result");


                                Appointment appt = new Appointment(scheduleId, diastolic.ToString(), systolic.ToString(), temperature.ToString(), pulse.ToString(), diagnosis, false, false, false, false, 0);
                                appt.DiagnosisId = diagnosisId;
                                appt.TestName = testname;
                                appt.TestDate = testdate;
                                appt.TestResult = testresult;
                                appt.Reason = reason;
                                appt.AppointmentDate = apptdate;

                                appts.Add(appt);
                            }                            
                        }
                    }
                }

                return appts;
            }
            catch
            {
                throw;
            }
            
        }

        /// <summary>
        /// Gets the appointment by a scheduleID.
        /// </summary>
        /// <param name="id">The scheduleID.</param>
        /// <returns>The corresponding Appointment object</returns>
        /// <exception cref="NotImplementedException"></exception>
        public override Appointment GetById(int id)
        {
            throw new NotImplementedException();
        }

        /// <summary>
        /// Gets all appointments.
        /// </summary>
        /// <returns>A list of all appointments</returns>
        /// <exception cref="NotImplementedException"></exception>
        public override IList<Appointment> GetAll()
        {
            throw new NotImplementedException();
        }
    }
}
