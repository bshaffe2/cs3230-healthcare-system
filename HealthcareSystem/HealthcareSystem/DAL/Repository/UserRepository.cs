﻿using HealthcareSystem.Base;
using HealthcareSystem.Model;
using MySql.Data.MySqlClient;
using System;
using System.Collections.Generic;

namespace HealthcareSystem.DAL.Repository
{
    class UserRepository : BaseRepository<User>
    {
        /// <summary>
        /// Adds the specified User to the list of users.
        /// </summary>
        /// <param name="entry">The entry.</param>
        /// <exception cref="NotImplementedException"></exception>
        public override void Add(User entry)
        {
            //Select from person to see if it exists and in nurse/admin
            //string userSQL = @"INSERT INTO users (personID, username, password) VALUES (@personID, @username, @password)";

            throw new NotImplementedException();
        }

        /// <summary>
        /// Gets the User by personID.
        /// </summary>
        /// <param name="id">The personID.</param>
        /// <returns>The corresponding User</returns>
        /// <exception cref="NotImplementedException"></exception>
        public override User GetById(int id)
        {
            throw new NotImplementedException();
        }

        /// <summary>
        /// Gets all Users.
        /// </summary>
        /// <returns>A list of Users</returns>
        /// <exception cref="NotImplementedException"></exception>
        public override IList<User> GetAll()
        {
            throw new NotImplementedException();
        }

        /// <summary>
        /// Checks if the User already exists, based on username and password.
        /// </summary>
        /// <param name="entry">The specified User.</param>
        /// <returns>If the User exists</returns>
        public bool UserExists(User entry)
        {
            string SQL = @"SELECT 1 FROM users u WHERE u.username = @username";
            bool userExists;

            try
            {
                using (MySqlConnection connection = DBConnection.GetConnection(this.ConnectionLabel))
                {
                    connection.Open();

                    using (MySqlCommand cmd = new MySqlCommand(SQL, connection))
                    {
                        cmd.Parameters.AddWithValue("@username", entry.Username);

                        var user = cmd.ExecuteScalar();
                        userExists = (user != null);
                    }
                }
            }
            catch (MySqlException e)
            {
                throw;
            }            

            return userExists;
        }

        /// <summary>
        /// Determines whether a username and password combination is a valid login.
        /// </summary>
        /// <param name="username">The username.</param>
        /// <param name="password">The password.</param>
        /// <returns>The user that corresponds to the username and password</returns>
        public User IsValidLogin(string username, string password)
        {
            string SQL = @" SELECT 
	                            p.personID, u.username, p.fname, p.lname,
                                CASE WHEN EXISTS (SELECT personID FROM administrator a WHERE u.personID = a.personID)
    	                            THEN 'Administrator'
                                ELSE 'Nurse'
                                END AS usertype
                            FROM users u JOIN person p USING (personID) WHERE u.username = @username AND u.password = @password";
            User validUser = null;

            try
            {
                using (MySqlConnection connection = DBConnection.GetConnection(this.ConnectionLabel))
                {
                    connection.Open();

                    using (MySqlCommand cmd = new MySqlCommand(SQL, connection))
                    {
                        cmd.Parameters.AddWithValue("@username", username);
                        cmd.Parameters.AddWithValue("@password", password);

                        using (MySqlDataReader reader = cmd.ExecuteReader())
                        {
                            while (reader.Read())
                            {
                                int personID = !reader.IsDBNull(0) ? reader.GetInt32(0) : 0;
                                string user = !reader.IsDBNull(1) ? reader.GetString(1) : "";
                                string fname = !reader.IsDBNull(2) ? reader.GetString(2) : "";
                                string lname = !reader.IsDBNull(3) ? reader.GetString(3) : "";
                                string type = !reader.IsDBNull(4) ? reader.GetString(4) : "";

                                PersonType userType = (type == "Administrator")
                                    ? PersonType.Administrator
                                    : PersonType.Nurse;

                                validUser = new User(fname, lname, default(DateTime), "", "", "", "", "", "", user, "", ' ', userType) {PersonId = personID};
                            }           
                        }
                    }
                }

                return validUser;
            }
            catch (MySqlException e)
            {
                throw;
            }            
        }
        
    }
}
