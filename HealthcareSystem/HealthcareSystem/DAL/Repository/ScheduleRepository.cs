﻿using System;
using System.Collections.Generic;
using HealthcareSystem.Base;
using HealthcareSystem.Model;
using MySql.Data.MySqlClient;

namespace HealthcareSystem.DAL.Repository
{
    public class ScheduleRepository : BaseRepository<Schedule>
    {
        /// <summary>
        /// Adds a specified scheduled appointment to the list.
        /// </summary>
        /// <param name="entry">The scheduled appointment to add.</param>
        public override void Add(Schedule entry)
        {
            string sql =
                @"INSERT INTO schedule (doctorID, datetime, patientID, reason) VALUES (@doctorID, @datetime, @patientID, @reason)";

            try
            {
                using (MySqlConnection connection = DBConnection.GetConnection(this.ConnectionLabel))
                {
                    connection.Open();

                    using (MySqlCommand cmd = new MySqlCommand(sql, connection))
                    {
                        cmd.Parameters.AddWithValue("@doctorID", entry.DoctorId);
                        cmd.Parameters.AddWithValue("@datetime", entry.Datetime);
                        cmd.Parameters.AddWithValue("@patientID", entry.PatientId);
                        cmd.Parameters.AddWithValue("@reason", entry.Reason);
                        cmd.ExecuteNonQuery();
                    }
                }
            }
            catch (MySqlException e)
            {
                throw;
            }
        }

        /// <summary>
        /// Gets all scheduled appointments.
        /// </summary>
        /// <returns>A list of all scheduled appointments</returns>
        public override IList<Schedule> GetAll()
        {
            string sql = @"
                SELECT 
	                doc.scheduleID, doc.doctorID, CONCAT(doc.lname, ', ', doc.fname) AS dname,
                    doc.datetime, pat.patientID, CONCAT(pat.lname, ', ', pat.fname) AS pname,
                    pat.reason
                FROM 
	                (
                        SELECT
     	                    *
                        FROM 
        	                schedule s
                            JOIN person p ON (p.personID = s.doctorID)
                    ) doc
                    JOIN
                    (
                        SELECT
        	                *
                        FROM
        	                schedule s
        	                JOIN person p ON (p.personID = s.patientID)
                    ) pat USING (scheduleID)
                WHERE pat.scheduleID NOT IN (SELECT scheduleID FROM appointment)";
            IList<Schedule> schedules = new List<Schedule>();
            try
            {
                using (MySqlConnection connection = DBConnection.GetConnection(this.ConnectionLabel))
                {
                    connection.Open();

                    using (MySqlCommand cmd = new MySqlCommand(sql, connection))
                    {
                        using (MySqlDataReader reader = cmd.ExecuteReader())
                        {
                            while (reader.Read())
                            {
                                int scheduleId = (!reader.IsDBNull(0) ? reader.GetInt32(0) : 0);
                                int doctorId = (!reader.IsDBNull(1) ? reader.GetInt32(1) : 0);
                                string dname = (!reader.IsDBNull(2) ? reader.GetString(2) : "");
                                DateTime datetime = (!reader.IsDBNull(3) ? reader.GetDateTime(3) : DateTime.MinValue);
                                int patientId = (!reader.IsDBNull(4) ? reader.GetInt32(4) : 0);
                                string pname = (!reader.IsDBNull(5) ? reader.GetString(5) : "");
                                string reason = (!reader.IsDBNull(6) ? reader.GetString(6) : "No Reason");

                                Schedule schedule = new Schedule(scheduleId, doctorId, datetime, patientId, reason);
                                schedule.Dname = dname;
                                schedule.Pname = pname;
                                schedules.Add(schedule);
                            }
                        }
                    }
                }

                return schedules;
            }
            catch (MySqlException e)
            {
                throw;
            }                
        }

        /// <summary>
        /// Gets the specified scheduled appointment by its schedule ID.
        /// </summary>
        /// <param name="id">The identifier.</param>
        /// <returns>The specific Schedule object</returns>
        /// <exception cref="NotImplementedException"></exception>
        public override Schedule GetById(int id)
        {
            throw new NotImplementedException();
        }
    }
}
