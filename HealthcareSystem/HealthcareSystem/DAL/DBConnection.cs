﻿using System;
using System.Configuration;
using MySql.Data.MySqlClient;

namespace HealthcareSystem.DAL
{
    public class DBConnection
    {
        /// <summary>
        /// Gets the connection information for hooking up the database to the code.
        /// </summary>
        /// <param name="connLabel">The connection label.</param>
        /// <returns></returns>
        public static MySqlConnection GetConnection(String connLabel)
        {
            string connString = ConfigurationManager.ConnectionStrings[connLabel].ConnectionString;
            return new MySqlConnection(connString);
        }
    }
}
