﻿using System.Linq;
using System.Text.RegularExpressions;

namespace HealthcareSystem.Utility
{
    /// <summary>
    /// Provides methods for validating forms
    /// </summary>
    public static class ValidateForm
    {
        /// <summary>
        /// Validates the name textbox for registration and patient information.
        /// </summary>
        /// <param name="name">The name.</param>
        /// <returns>If the name value is valid. </returns>
        public static bool ValidateName(string name)
        {
            return name.Length != 0;
        }

        /// <summary>
        /// Validates the zip code for registration and patient information.
        /// </summary>
        /// <param name="zip">The zip code.</param>
        /// <returns>If the zip code value is valid.</returns>
        public static bool ValidateZipCode(string zip)
        {
            Regex rx = new Regex(@"\d{5}");
                return rx.IsMatch(zip);
        }

        /// <summary>
        /// Validates the phone number for registration and patient information.
        /// </summary>
        /// <param name="phoneNumber">The phone number.</param>
        /// <returns>If the phone number value is valid</returns>
        public static bool ValidatePhoneNumber(string phoneNumber)
        {
            return !phoneNumber.Any(c => c < '0' || c > '9') && phoneNumber.Length == 10;
        }

        /// <summary>
        /// Validates the address for registration and patient information.
        /// </summary>
        /// <param name="address">The address.</param>
        /// <returns>If the address value is valid.</returns>
        public static bool ValidateAddress(string address)
        {
            return !string.IsNullOrEmpty(address);
        }

        /// <summary>
        /// Validates the state for registration and patient information.
        /// </summary>
        /// <param name="state">The state.</param>
        /// <returns>If the state value is valid.</returns>
        public static bool ValidateState(string state)
        {
            return !string.IsNullOrEmpty(state);
        }

        /// <summary>
        /// Validates the checkup entry values for appointments.
        /// </summary>
        /// <param name="entry">The checkup entry.</param>
        /// <returns>If the checkup value is valid.</returns>
        public static bool ValidateCheckEntry(string entry)
        {
            Regex rx = new Regex(@"\d");
            return rx.IsMatch(entry) &&  !string.IsNullOrEmpty(entry);
        }
    }
}
